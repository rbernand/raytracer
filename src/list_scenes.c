/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_scenes.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbenand@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/10 14:55:56 by rbernand          #+#    #+#             */
/*   Updated: 2015/09/10 15:03:02 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <rt.h>
#include <stdio.h>

t_return				list_scenes(char **cmd)
{
	t_scene				*scn;

	(void)cmd;
	scn = get_env(NULL)->scenes;
	while (scn)
	{
		printf("\t%lud. %s\n", scn->win.id, scn->name);
		scn = scn->next;
	}
	return (_SUCCESS);
}
