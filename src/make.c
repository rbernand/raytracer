/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/10 10:28:46 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 07:14:39 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <parser.h>
#include <prompt.h>
#include <mlx.h>
#include <rt.h>

static void		add_scene(t_scene **scn, t_scene *new)
{
	t_scene			*tmp;

	if (*scn == NULL)
		*scn = new;
	else
	{
		tmp = *scn;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
}

static int		expose_hook(t_scene *scn)
{
	mlx_put_image_to_window(get_mlx(NULL)->ptr, scn->win.ptr, scn->imgs, 0, 0);
	return (1);
}

t_return		make(char **cmd)
{
	t_scene			*scn;

	if (cmd[0] == NULL)
		return (PERROR("usage: make input_file"));
	if ((scn = create_scene(cmd[0])) == NULL)
		return (PERROR("create_scene: cannot create scene"));
	ft_putendl("\033[1mScene Loaded.\033[0m");
	add_scene(&get_env(NULL)->scenes, scn);
	if (get_env(NULL)->verbose)
		scn->dump(scn);
	render(get_env(NULL), scn);
	if (mlx_create_window(scn) == _ERR)
		return (PERROR("failed to get window."));
	mlx_expose_hook(scn->win.ptr, expose_hook, scn);
	mlx_display_result(get_mlx(NULL), scn);
	return (_SUCCESS);
}
