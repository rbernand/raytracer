/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reflection_factory.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/27 11:15:25 by rbernand          #+#    #+#             */
/*   Updated: 2015/08/27 11:44:29 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <parser.h>

#define MIN(X,Y)		((X) < (Y) ? (X) : (Y))
#define MAX(X,Y)		((X) > (Y) ? (X) : (Y))

t_return		reflection_factory(t_parser *elem, char *str)
{
	while (*str && !ft_isdigit(*str))
		str++;
	elem->reflection = MIN(MAX(ft_atoi(str), 0), 100);
	return (_SUCCESS);
}
