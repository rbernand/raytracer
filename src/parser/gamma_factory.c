/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gamma_factory.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 16:11:48 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/07 16:13:54 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>
#include <math.h>
#include <libvect.h>
#include <parser.h>

#define MIN(X,Y)		((X) < (Y) ? (X) : (Y))
#define MAX(X,Y)		((X) > (Y) ? (X) : (Y))
#define MAX_SAMPLING	10000

t_return	gamma_factory(t_parser *elem, char *str)
{
	while (str && (!ft_isdigit(*str) && *str != '.'))
		str++;
	elem->gamma = MIN(MAX(atof(str), 0.5), 1.5);
	return (_SUCCESS);
}
