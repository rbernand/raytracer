/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   deepth_factory.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/04 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2016/02/04 12:52:04 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <math.h>
#include <libvect.h>
#include <parser.h>

#define MIN(X,Y)		((X) < (Y) ? (X) : (Y))
#define MAX(X,Y)		((X) > (Y) ? (X) : (Y))
#define MAX_SAMPLING	10000

t_return	depth_factory(t_parser *elem, char *str)
{
	while (str && (!ft_isdigit(*str) && *str != '.'))
		str++;
	elem->depth = MIN(MAX(ft_atoi(str), 0), MAX_SAMPLING);
	return (_SUCCESS);
}
