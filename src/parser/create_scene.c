/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_scene.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/10 10:37:58 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/05 14:23:42 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>
#include "libft.h"
#include "list.h"
#include "mymlx.h"
#include "parser.h"
#include "rt.h"

#define E_BAR		"[                                                   ]\r"
#define E_BAR_SIZE	(sizeof(E_BAR))

static void			dump(t_scene *scn)
{
	t_element			*elem;

	printf("====================================\n");
	printf("\t\033[35m'%s'\033[0m\n", scn->name);
	printf("\033[1mWidth:\033[0m%10lu\n", scn->win_x);
	printf("\033[1mHeight:\033[0m%9lu\n", scn->win_y);
	printf("\033[1mDepth:\033[0m%10lu\n", scn->depth);
	printf("\033[1mSampling:\033[0m%7lu\n", scn->sampling);
	printf("\033[1mThreads:\033[0m%8lu\n", scn->threads);
	printf("====================================\n");
	elem = scn->elems;
	while (elem)
	{
		elem->dump(elem);
		elem = elem->next;
	}
	printf("\t\033[11mCamera\t\t\t\033[0m\n");
	printf("\033[11mPostion:\t\t\t\033[0m%s\n", str_v3f(scn->cam.ori));
	printf("\033[11mDirection:\t\t\t\033[0m%s\n", str_v3f(scn->cam.dir));
}

static void			put_percent_bar(t_scene *scn)
{
	static char		moon[] = {'|', '\\', '-', '/', '|', '\\', '-', '/'};
	static char		buf[E_BAR_SIZE] = E_BAR;

	if (scn->counter == 0)
		ft_memcpy(buf, E_BAR, E_BAR_SIZE);
	scn->counter++;
	if (scn->counter % (scn->win_y / 50) == 0)
	{
		buf[scn->counter / (scn->win_y / 50)] = '=';
		ft_putstr(buf);
	}
	buf[scn->counter / (scn->win_y / 50) + 1] =
		moon[scn->counter % 8];
	ft_putstr(buf);
}

static void			free_scene(t_scene *scn)
{
	free(scn->name);
	while (scn->elems)
		LIST_DELETE(&scn->elems, scn->elems, free);
	while (scn->groups)
		LIST_DELETE(&scn->groups, scn->groups, free);
	while (scn->imgs)
		LIST_DELETE(&scn->imgs, scn->imgs, free);
	while (scn->groups)
		LIST_DELETE(&scn->groups, scn->groups, free);
}

t_scene				*create_scene(char *input_file)
{
	t_scene			*scn;

	if ((scn = (t_scene *)malloc(sizeof(t_scene))) == NULL)
		return (NULL);
	bzero(scn, sizeof(t_scene));
	if ((scn->elems = load_scene(input_file, scn)) == NULL)
		return (NULL);
	scn->dump = &dump;
	scn->put_bar = &put_percent_bar;
	scn->free_scn = &free_scene;
	if ((mlx_create_image(&get_env(NULL)->mlx, scn)) == _ERR)
	{
		PERROR("Failed to create image");
		return (NULL);
	}
	return (scn);
}
