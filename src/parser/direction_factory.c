/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   direction_factory.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/04 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2016/03/05 14:21:06 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <math.h>
#include <libvect.h>
#include <parser.h>

t_return	direction_factory(t_parser *elem, char *str)
{
	while (*str && *str != '-' && !ft_isdigit(*str))
		str++;
	elem->dir = vecteur_factory(str);
	return (_SUCCESS);
}
