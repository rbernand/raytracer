/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transparency_factory.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbenand@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/24 13:04:13 by rbernand          #+#    #+#             */
/*   Updated: 2015/09/24 13:09:00 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <parser.h>

#define MIN(X,Y)		((X) < (Y) ? (X) : (Y))
#define MAX(X,Y)		((X) > (Y) ? (X) : (Y))

t_return		transparency_factory(t_parser *elem, char *str)
{
	while (*str && !ft_isdigit(*str))
		str++;
	elem->transparency = MIN(MAX(atoi(str), 0), 100);
	return (_SUCCESS);
}
