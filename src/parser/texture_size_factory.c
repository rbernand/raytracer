/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture_size_factory.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 11:27:17 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/05 14:20:44 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <libvect.h>
#include <parser.h>

t_return	texture_size_factory(t_parser *elem, char *str)
{
	while (*str && (!ft_isdigit(*str) && *str != '-' && *str != '.'))
		str++;
	elem->texture_size = vecteur_factory(str);
	if (elem->texture_size.x == 0 || elem->texture_size.y == 0)
		return (PERROR("invalid Texture size"));
	return (_SUCCESS);
}
