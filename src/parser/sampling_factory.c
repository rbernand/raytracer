/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sampling_factory.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/04 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2015/09/04 15:56:25 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <math.h>
#include <libvect.h>
#include <parser.h>

#define MIN(X,Y)		((X) < (Y) ? (X) : (Y))
#define MAX(X,Y)		((X) > (Y) ? (X) : (Y))
#define MAX_SAMPLING	32

t_return	sampling_factory(t_parser *elem, char *str)
{
	while (str && (!ft_isdigit(*str) && *str != '.'))
		str++;
	elem->sampling = MIN(MAX(ft_atoi(str), 1), MAX_SAMPLING);
	return (_SUCCESS);
}
