/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_factory.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/04 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2016/03/05 12:21:09 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <parser.h>

t_return			texture_factory(t_parser *elem, char *str)
{
	while (str && *str != '"')
		str++;
	elem->texture = ft_strdup(++str);
	elem->texture[ft_strlen(elem->texture) - 1] = 0;
	return (_SUCCESS);
}
