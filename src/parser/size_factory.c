/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   size_factory.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmerlier <tmerlier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/04 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2015/06/09 15:45:37 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <libvect.h>
#include <parser.h>

t_return	size_factory(t_parser *elem, char *str)
{
	while (*str && (!ft_isdigit(*str) && *str != '-' && *str != '.'))
		str++;
	elem->size = vecteur_factory(str);
	return (_SUCCESS);
}
