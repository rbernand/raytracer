/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   diffuse_color_factory.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/04 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2015/06/07 19:53:54 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <libvect.h>
#include <parser.h>

t_return	specular_color_factory(t_parser *elem, char *str)
{
	while (*str && !ft_isdigit(*str))
		str++;
	elem->specular_color = color_factory(str);
	return (_SUCCESS);
}
