/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_scenes_by_id.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbenand@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/01 12:44:18 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/02 21:07:03 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <parser.h>
#include <prompt.h>
#include <rt.h>

t_scene			*get_scenes_by_id(size_t id)
{
	t_scene		*scn;

	scn = get_env(NULL)->scenes;
	while (scn != NULL)
	{
		if (scn->id == id)
			return (scn);
		scn = scn->next;
	}
	return (NULL);
}
