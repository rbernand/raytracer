/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_color_factory.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/04 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2015/06/06 18:44:59 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdlib.h>
#include <libvect.h>
#include <mymlx.h>
#include <libft.h>

#include <stdio.h>

#define MIN(X,Y)		((X) < (Y) ? (X) : (Y))
#define MAX(X,Y)		((X) > (Y) ? (X) : (Y))

t_color		get_color_factory(char *str)
{
	t_color		value;

	value.rgba.r = MIN(MAX(atoi(str), 0), 255);
	while (str && ft_isdigit(*str))
		str++;
	value.rgba.g = MIN(MAX(atoi(str++), 0), 255);
	while (str && ft_isdigit(*str))
		str++;
	value.rgba.b = MIN(MAX(atoi(str++), 0), 255);
	while (str && ft_isdigit(*str))
		str++;
	value.rgba.a = MIN(MAX(atoi(str), 0), 100);
	return (value);
}
