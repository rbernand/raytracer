/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_factory.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/08 18:37:21 by rbernand          #+#    #+#             */
/*   Updated: 2015/08/23 20:03:36 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <parser.h>

t_return			file_factory(t_parser *elem, char *str)
{
	while (str && *str != '"')
		str++;
	elem->file = ft_strdup(++str);
	elem->file[ft_strlen(elem->file) - 1] = 0;
	return (_SUCCESS);
}
