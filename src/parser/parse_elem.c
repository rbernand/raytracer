/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_elem.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/04 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2016/03/07 16:11:37 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "parser.h"

static t_factory		g_factory[_MAX_TYPE_PARAM] = {
	&center_factory,
	&size_factory,
	&radius_factory,
	&ambiante_color_factory,
	&diffuse_color_factory,
	&specular_color_factory,
	&direction_factory,
	&name_factory,
	&file_factory,
	&texture_factory,
	&reflection_factory,
	&refraction_factory,
	&transparency_factory,
	&sampling_factory,
	&depth_factory,
	&threads_factory,
	&texture_size_factory,
	&bump_factory,
	&gamma_factory
};

static t_parser			*new_parser(t_type_parser type)
{
	t_parser *new;

	if (!(new = (t_parser *)malloc(sizeof(t_parser))))
		PERROR("malloc : Can not allocated new_parser");
	ft_bzero(new, sizeof(t_parser));
	new->type = type;
	new->refraction = 1.0;
	return (new);
}

static t_type_param		get_param(const char *str)
{
	int					i;
	const char			*g_params[_MAX_TYPE_PARAM] = {
	"center", "size", "radius", "ambiant_color",
	"diffuse_color", "specular_color", "direction", "name", "file", "texture",
	"reflection", "refraction", "transparency", "sampling", "depth",
	"threads", "siz_tex", "bump"
	};

	i = -1;
	while (++i < _MAX_TYPE_PARAM)
	{
		if (ft_strnequ(g_params[i], str, ft_strlen(g_params[i])))
			return (i);
	}
	ft_putstr("\033[31;4mLine invalid: \033[0m");
	ft_putendl(str);
	return (_INVALID);
}

t_parser				*parse_elem(int fd, t_type_parser type)
{
	char					*line;
	t_type_param			param;
	t_parser				*elem;

	elem = new_parser(type);
	while (get_next_line(fd, &line) && line[0] != '!')
	{
		if (!(line && (line[0] == 0 || ft_jumpstr(line)[0] == '#')))
		{
			param = get_param(ft_jumpstr(line));
			if (param >= 0)
				g_factory[param](elem, line);
		}
		ft_strdel(&line);
	}
	ft_strdel(&line);
	return (elem);
}
