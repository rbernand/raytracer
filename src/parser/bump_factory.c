/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bump_factory.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/06 15:24:43 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/06 15:25:10 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <parser.h>

t_return			bump_factory(t_parser *elem, char *str)
{
	while (str && *str != '"')
		str++;
	elem->bump = ft_strdup(++str);
	elem->bump[ft_strlen(elem->bump) - 1] = 0;
	return (_SUCCESS);
}
