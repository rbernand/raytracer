/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_elements_from_parser.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmerlier <tmerlier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/24 12:04:36 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 11:19:55 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "parser.h"
#include "elements.h"
#include "list.h"

t_element			*add_blob(t_element **objs, t_element *new)
{
	t_element			*blob;
	t_element			*tmp;

	tmp = *objs;
	while (tmp)
	{
		if (tmp->type == _BLOB)
			blob = tmp;
		tmp = tmp->next;
	}
	if (blob)
	{
		PUSH_FRONT(&blob->blobs, new);
		return (NULL);
	}
	PUSH_FRONT(objs, new_blob(
			NULL_V3F, NULL_V3F, NULL_V3F, new->ambiante_color));
	(*objs)->blobs = new;
	return (NULL);
}

t_element			*new_elemts_from_parser(t_parser *lst,
						t_texture *textures, t_element **objs)
{
	t_element						*new;
	static t_elem_constructor		constructor[_MAX_TYPE_ELEM] = {
	&new_sphere, &new_spot, &new_plane, &new_cylinder, &new_cone, &new_disk,
	&new_sphere
	};

	lst->dir = prod_v3f(lst->dir, M_PI / 180);
	new = constructor[lst->type](lst->center,
			lst->dir,
			lst->size,
			lst->ambiante_color);
	new->diffuse_color = lst->diffuse_color;
	new->specular_color = lst->specular_color;
	new->size = lst->size;
	new->name = lst->name;
	new->reflection = lst->reflection;
	new->refraction = lst->refraction;
	new->transparency = lst->transparency;
	if (lst->type == _BLOB)
		return (add_blob(objs, new));
	link_elements(new, lst, textures);
	return (new);
}
