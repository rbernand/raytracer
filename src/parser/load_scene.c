/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_scene.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/28 14:29:10 by etermeau          #+#    #+#             */
/*   Updated: 2016/03/09 00:53:27 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <fcntl.h>
#include <elements.h>
#include <libft.h>
#include <parser.h>

static void				add_parser(t_parser **list, t_parser *new)
{
	static t_parser		*last = NULL;

	if (*list == NULL)
		*list = new;
	else
		last->next = new;
	last = new;
}

static t_type_parser	get_status(char *str)
{
	int					i;
	static const char	*balises[_MAX_TYPE_PARSER] = {
	"+sphere",
	"+spot",
	"+plane",
	"+cylinder",
	"+cone",
	"+disk",
	"+blob",
	"+cube",
	"+config",
	"+point",
	"+camera",
	"+texture",
	"+group",
	};

	i = -1;
	while (++i < _MAX_TYPE_PARSER)
		if (ft_strequ(balises[i], str))
			return (i);
	ft_putstr("\033[31;4mLine invalid:\033[0m\t");
	ft_putendl(str);
	return (_WAITING);
}

static int				open_file(char *file)
{
	int	fd;

	fd = open(file, O_RDONLY);
	if (fd < 0)
		return (-1);
	return (fd);
}

t_element				*load_scene(char *file, t_scene *scn)
{
	int					fd;
	char				*line;
	t_type_parser		status;
	t_parser			*list;

	ft_putendl("\033[1mLoading scene.\033[0m");
	if ((fd = open_file(file)) < 0)
	{
		PERROR("open: failed to open file");
		return (NULL);
	}
	status = _WAITING;
	list = NULL;
	line = NULL;
	while (get_next_line(fd, &line) > 0)
	{
		if (!(line && (line[0] == 0 || ft_jumpstr(line)[0] == '#')))
			if ((status = get_status(line)) != _WAITING)
				add_parser(&list, parse_elem(fd, status));
		ft_strdel(&line);
	}
	ft_strdel(&line);
	close(fd);
	return (convert_parser(scn, list, &scn->cam, &scn->textures));
}
