/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   link_elements.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/09 12:10:49 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/06 16:08:09 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <elements.h>
#include <parser.h>

t_return			link_elements(t_element *element,
					t_parser *parser, t_texture *textures)
{
	if (!parser->texture && !parser->bump)
		return (_ERR);
	while (textures)
	{
		if (ft_strequ(parser->texture, textures->name))
		{
			element->texture = textures;
			element->texture_size = parser->texture_size;
			if (element->texture_size.x == 0
					&& element->texture_size.y == 0
					&& element->texture_size.z == 0)
				element->texture_size = (t_vect3f){1.0, 1.0, 1.0};
		}
		if (ft_strequ(parser->bump, textures->name))
			element->bump = textures;
		textures = textures->next;
	}
	return (_ERR);
}
