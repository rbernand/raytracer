/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vecteur_factory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/05 13:22:47 by etermeau          #+#    #+#             */
/*   Updated: 2015/08/23 20:54:38 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libvect.h>
#include <stdlib.h>
#include <libft.h>

t_vect3f			vecteur_factory(char *str)
{
	t_vect3f	value;

	value.x = atof(str);
	while (str && (ft_isdigit(*str) || *str == '.' || *str == '-'))
		str++;
	value.y = atof(++str);
	while (str && (ft_isdigit(*str) || *str == '.' || *str == '-'))
		str++;
	value.z = atof(++str);
	return (value);
}
