/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_parser.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/05 17:11:33 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 03:30:04 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <parser.h>
#include <math.h>
#include <elements.h>
#include <list.h>
#include <rt.h>

static void				add_texture(t_texture **lst, t_parser *texture)
{
	t_texture			*new;
	static t_texture	*last = NULL;

	if ((new = (t_texture *)malloc(sizeof(t_texture))) == NULL)
	{
		PERROR("malloc: failed");
		return ;
	}
	new->name = texture->name;
	new->input_file = texture->file;
	if (load_texture(get_mlx(0), new) == _ERR)
		ft_strdel(&new->name);
	new->next = NULL;
	if (*lst == NULL)
		*lst = new;
	else
		last->next = new;
	last = new;
}

static void				set_camera(t_parser *elem, t_cam *cam)
{
	if (!cam)
		return ;
	cam->ori = elem->center;
	cam->dir.x = elem->dir.x * M_PI / 180.;
	cam->dir.y = elem->dir.y * M_PI / 180.;
	cam->dir.z = elem->dir.z * M_PI / 180.;
}

static void				set_config(t_parser *elem, t_scene *scn)
{
	if (!elem)
	{
		scn->name = ft_strdup("Untitled");
		scn->win_x = 1280;
		scn->win_y = 1024;
		scn->sampling = 1;
		scn->depth = 10;
		scn->threads = 1;
		scn->gamma = 1;
	}
	else
	{
		scn->name = elem->name ? ft_strdup(elem->name) : ft_strdup("Untitled");
		scn->win_x = elem->size.x ? elem->size.x : 1280;
		scn->win_y = elem->size.y ? elem->size.y : 1024;
		scn->sampling = elem->sampling ? elem->sampling : 1;
		scn->depth = elem->depth ? elem->depth : 10;
		scn->threads = elem->threads ? elem->threads : 1;
		scn->gamma = elem->gamma ? elem->gamma : 1;
		free(elem->name);
	}
}

t_element				*convert_parser(t_scene *scn, t_parser *lst,
						t_cam *cam, t_texture **texs)
{
	t_element			*objs;
	t_parser			*todel;

	objs = NULL;
	set_config(NULL, scn);
	while (lst)
	{
		todel = lst;
		if (lst->type < (t_type_parser)_MAX_TYPE_ELEM)
			PUSH_BACK(&objs, new_elemts_from_parser(lst, *texs, &objs));
		else if (lst->type == _CAMERA)
			set_camera(lst, cam);
		else if (lst->type == _CONFIG)
			set_config(lst, scn);
		else if (lst->type == _TEXTURE)
			add_texture(texs, lst);
		lst = lst->next;
		free(todel);
	}
	if (!scn->name)
		scn->name = "Untitled Scene";
	return (objs);
}
