/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   center_factory.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/04 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2015/06/06 18:47:47 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <libvect.h>
#include <parser.h>

t_return	center_factory(t_parser *elem, char *str)
{
	while (*str && (!ft_isdigit(*str) && *str != '-'))
		str++;
	elem->center = vecteur_factory(str);
	return (_SUCCESS);
}
