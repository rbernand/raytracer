/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   threads_factory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/01 17:28:04 by vcosson           #+#    #+#             */
/*   Updated: 2015/10/01 17:35:45 by vcosson          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <parser.h>

#define MAX(X,Y)		((X) > (Y) ? (X) : (Y))

t_return	threads_factory(t_parser *elem, char *str)
{
	while (str && (!ft_isdigit(*str)))
		str++;
	elem->threads = MAX(ft_atoi(str), 1);
	return (_SUCCESS);
}
