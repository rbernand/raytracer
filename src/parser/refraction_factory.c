/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   refraction_factory.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbenand@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/05 15:36:52 by rbernand          #+#    #+#             */
/*   Updated: 2015/09/05 16:06:20 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <parser.h>

#define MIN(X,Y)		((X) < (Y) ? (X) : (Y))
#define MAX(X,Y)		((X) > (Y) ? (X) : (Y))

t_return		refraction_factory(t_parser *elem, char *str)
{
	while (*str && !ft_isdigit(*str))
		str++;
	elem->refraction = MIN(MAX(atof(str), 0.0), 2.0);
	return (_SUCCESS);
}
