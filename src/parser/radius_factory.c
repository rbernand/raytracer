/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   radius_factory.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/04 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2015/08/23 20:04:06 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <parser.h>

t_return			radius_factory(t_parser *elem, char *str)
{
	while (str && (!ft_isdigit(*str) && *str != '.'))
		str++;
	elem->size.x = atof(str);
	return (_SUCCESS);
}
