/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   name_factory.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/07 21:37:54 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/03 17:50:08 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <parser.h>

t_return			name_factory(t_parser *elem, char *str)
{
	if (!ft_strchr(str, '"') || ft_strchr(str, '"') == ft_strrchr(str, '"'))
		return (PERROR("invalid text argument"));
	while (str && *str != '"')
		str++;
	elem->name = ft_strdup(++str);
	elem->name[ft_strlen(elem->name) - 1] = 0;
	return (_SUCCESS);
}
