/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmerlier <tmerlier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/05 13:27:30 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 12:17:01 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <mymlx.h>
#include <rt.h>

t_color				get_color(t_element *elem, t_element *elems,
						t_vect3f inter)
{
	t_color				color;
	t_color				shadow;
	t_element			*tmp;

	color = elem->get_color(elem, inter);
	color = prod_color(color, elem->ambiante_color.rgba.a / 100.0);
	tmp = elems;
	while (tmp)
	{
		if (tmp->type == _SPOT)
		{
			if ((shadow = get_shadow(elems, elem, tmp, inter)).rgba.a)
				color = prod_color(color, 1. - shadow.rgba.a / 100.0);
			else if (dot_product_v3f(sub_v3f(tmp->center, elem->center),
					elem->normale(elem, inter)) > 0
					&& !shadow.rgba.a)
				color = mix_color(color,
						phong_shading(elem, tmp, inter, color),
						1. - shadow.rgba.a / 100., 1. - shadow.rgba.a / 100.);
		}
		tmp = tmp->next;
	}
	color.rgba.a = 0;
	return (sum_color(color, prod_color(elem->get_color(elem, inter),
					elem->ambiante_color.rgba.a / 100.0)));
}
