/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filter.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/25 17:22:06 by etermeau          #+#    #+#             */
/*   Updated: 2016/03/07 12:24:08 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <filter.h>
#include <rt.h>
#include <libft.h>
#include <prompt.h>
#include <stdio.h>

const char		*g_filter_name[_MAX_FILTER] = {
	"blur",
	"motion_blur",
	"sharpen"
};

static enum e_filter	get_filter_id(char const *str)
{
	int		i;

	i = -1;
	while (++i < _MAX_FILTER)
		if (ft_strequ(g_filter_name[i], str))
			return (i);
	return (_INVALID);
}

static t_return			print_usage(void)
{
	int		i;

	ft_putendl("Usage: 'filter' 'id' 'name of filter'");
	ft_putendl("filter :");
	i = 0;
	while (i < _MAX_FILTER)
	{
		printf("\t- %s\n", g_filter_name[i]);
		i++;
	}
	return (_ERR);
}

t_return				filter(char **cmd)
{
	t_scene			*scn;
	enum e_filter	id;

	if (cmd[0] == NULL || cmd[1] == NULL)
		return (print_usage());
	scn = get_scenes_by_id(ft_atoi(cmd[0]));
	id = get_filter_id(cmd[1]);
	apply_filter(scn, id);
	if (scn == NULL || id == _INVALID)
		return (print_usage());
	return (_SUCCESS);
}
