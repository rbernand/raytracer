/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   apply_filter.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/04 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2016/03/07 13:09:43 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <mlx.h>
#include <mymlx.h>
#include <rt.h>
#include <filter.h>

const t_filter		g_filter_list[_MAX_FILTER] = {
	{.width = 3, .height = 3, .factor = 1., .bias = 0., .value = {
			{0., 0.2, 0.},
			{0.2, 0.2, 0.2},
			{0., 0.2, 0.}
		}
	},
	{.width = 9, .height = 9, .factor = 1.0 / 9.0, .bias = .0, .value = {
			{1, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 1, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 1, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 1, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 1, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 1, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 1, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 1, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 1}
		}
	},
	{.width = 3, .height = 3, .factor = 1., .bias = 0., .value = {
			{-1., -1., -1.},
			{1., 9., -1.},
			{-1., -1., -1.}
		}
	}
};

static double		*get_pixel_color(double *rgb, t_point pixel,
						t_scene *scn, t_filter filter)
{
	t_color		color;
	size_t		filter_x;
	size_t		filter_y;
	int			image_x;
	int			image_y;

	filter_x = 0;
	while (filter_x < filter.width)
	{
		image_x = pixel.x - filter.width / 2 + filter_x;
		filter_y = 0;
		while (filter_y < filter.height)
		{
			image_y = pixel.y - filter.height / 2 + filter_y;
			color = mlx_get_value(scn->imgs->next, image_x, image_y);
			rgb[0] += (float)color.rgba.r * filter.value[filter_x][filter_y];
			rgb[1] += (float)color.rgba.g * filter.value[filter_x][filter_y];
			rgb[2] += (float)color.rgba.b * filter.value[filter_x][filter_y];
			filter_y++;
		}
		filter_x++;
	}
	return (rgb);
}

void				apply_filter(t_scene *scn, enum e_filter id)
{
	t_img		*img;
	t_point		pixel;
	t_filter	filter;
	double		rgb[3];

	filter = g_filter_list[id];
	mlx_create_image(get_mlx(NULL), scn);
	img = scn->imgs;
	pixel.y = 0;
	while (++pixel.y < scn->win_y - 1 && (pixel.x = 0) == 0)
		while (++pixel.x < scn->win_x - 1)
		{
			bzero(rgb, sizeof(double) * 3);
			get_pixel_color(rgb, pixel, scn, filter);
			pixel.color.rgba.r = FILTER(0,
					(int)(filter.factor * rgb[0] + filter.bias), 255);
			pixel.color.rgba.g = FILTER(0,
					(int)(filter.factor * rgb[1] + filter.bias), 255);
			pixel.color.rgba.b = FILTER(0,
					(int)(filter.factor * rgb[2] + filter.bias), 255);
			pixel.color.rgba.a = 0;
			mlx_put_point(img, &pixel);
		}
}
