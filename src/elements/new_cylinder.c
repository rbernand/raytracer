/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_cylinder.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 12:44:40 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/06 18:07:02 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdlib.h>
#include <libvect.h>
#include <libft.h>
#include <rt.h>
#include <elements.h>

static t_vect3f		uv_mapping(t_element *self, t_vect3f inter,
						t_vect3f normale, char is_bump)
{
	t_vect3f			xy;
	float				u;
	t_texture			*texture;

	texture = is_bump ? self->bump : self->texture;
	normale = rotate(normale, self->dir);
	inter = rotate(inter, self->dir);
	u = 0.5 + atan2(normale.y, normale.x) / (2 * M_PI);
	xy.x = (int)(texture->img.size_xy[0] * (u - (int)(u)));
	xy.y = -(int)(inter.z / self->texture_size.z) % texture->img.size_xy[1];
	if (xy.y < 0)
		xy.y += texture->img.size_xy[1];
	return (xy);
}

static t_vect3f		normale(t_element *self, t_vect3f inter)
{
	t_vect3f				normale;

	inter = sub_v3f(inter, self->center);
	inter = rotate(inter, self->dir);
	inter.z = 0;
	normale = inter;
	normalize_v3f(&normale);
	normale = rotate_neg(normale, self->dir);
	return (normale);
}

static t_return		intersect(t_element *cylinder, t_cam cam, float *dist)
{
	float				abc[3];
	float				delta;
	float				x[2];

	abc[0] = cam.dir.x * cam.dir.x + cam.dir.y * cam.dir.y;
	abc[1] = 2 * (cam.dir.x * cam.ori.x + cam.dir.y * cam.ori.y);
	abc[2] = cam.ori.x * cam.ori.x + cam.ori.y * cam.ori.y
		- cylinder->size.x * cylinder->size.x;
	delta = abc[1] * abc[1] - 4 * abc[0] * abc[2];
	if (delta < 0)
		return (_ERR);
	x[0] = (-abc[1] - sqrt(delta)) / (2 * abc[0]);
	x[1] = (-abc[1] + sqrt(delta)) / (2 * abc[0]);
	if (!((x[0] < x[1] || x[1] < 0) && x[0] > 0))
		return (_ERR);
	if (cylinder->size.z <= 0
			|| fabs(cam.dir.z * x[0] + cam.ori.z) < cylinder->size.z)
		*dist = x[0];
	else if (cylinder->size.z <= 0
			|| fabs(cam.dir.z * x[1] + cam.ori.z) < cylinder->size.z)
		*dist = x[1];
	else
		return (_ERR);
	return (_SUCCESS);
}

t_element			*new_cylinder(t_vect3f center, t_vect3f dir,
					t_vect3f size, t_color color)
{
	t_element			*new;

	new = new_element(_CYLINDER);
	new->center = center;
	new->dir = dir;
	new->size = size;
	new->ambiante_color = color;
	new->intersect = &intersect;
	new->normale = &normale;
	new->uv_mapping = &uv_mapping;
	return (new);
}
