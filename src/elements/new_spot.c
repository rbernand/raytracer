/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_spot.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 12:44:40 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/02 20:50:43 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdlib.h>
#include <libvect.h>
#include <libft.h>
#include <rt.h>
#include <elements.h>

static t_color		texture(t_element *self, t_vect3f inter)
{
	(void)inter;
	return (self->ambiante_color);
}

static void			dump(t_element *self)
{
	ft_putstr("\033[1m");
	if (self->name)
		printf("\tSpot: %s\n", self->name);
	else
		printf("\tSpot\n");
	ft_putstr("\033[0m");
	printf("center\t\t\t\t%s\n", str_v3f(self->center));
	printf("emission_color\t\t\t%s\n", str_color(self->emission_color));
}

t_element			*new_spot(t_vect3f center, t_vect3f dir,
					t_vect3f size, t_color color)
{
	t_element			*new;

	new = new_element(_SPOT);
	new->size = size;
	new->type = _SPOT;
	new->center = center;
	new->dir = dir;
	new->emission_color = color;
	new->intersect = NULL;
	new->dump = &dump;
	new->get_color = &texture;
	return (new);
}
