/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_cone.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 12:44:40 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/06 18:08:02 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdlib.h>
#include <libvect.h>
#include <libft.h>
#include <rt.h>
#include <elements.h>

static t_vect3f		uv_mapping(t_element *self, t_vect3f inter,
						t_vect3f normale, char is_bump)
{
	t_vect3f			xy;
	t_texture			*texture;

	texture = is_bump ? self->bump : self->texture;
	(void)normale;
	xy.x = (int)(inter.x / self->texture_size.x) % texture->img.size_xy[0];
	xy.y = -(int)(inter.y / self->texture_size.y) % texture->img.size_xy[1];
	if (xy.x < 0)
		xy.x += texture->img.size_xy[0];
	if (xy.y < 0)
		xy.y += texture->img.size_xy[1];
	return (xy);
}

static t_vect3f		normale(t_element *self, t_vect3f inter)
{
	static t_vect3f			normale = {0, 0, -1};

	(void)inter;
	return (rotate(normale, self->dir));
}

static t_return		intersect(t_element *disk, t_cam cam, float *dist)
{
	double				x;
	t_vect3f			inter;

	x = -cam.ori.z / cam.dir.z;
	inter.x = (cam.dir.x * x + cam.ori.x) / disk->size.x;
	inter.y = (cam.dir.y * x + cam.ori.y) / disk->size.y;
	inter.z = (cam.dir.z * x + cam.ori.z) / disk->size.z;
	if (get_norme_v3f(inter) < 1)
	{
		*dist = x;
		return (_SUCCESS);
	}
	return (_ERR);
}

t_element			*new_disk(t_vect3f center, t_vect3f dir,
					t_vect3f size, t_color color)
{
	t_element			*new;

	new = new_element(_DISK);
	new->center = center;
	new->dir = dir;
	new->size = size;
	new->ambiante_color = color;
	new->intersect = &intersect;
	new->normale = &normale;
	new->uv_mapping = &uv_mapping;
	return (new);
}
