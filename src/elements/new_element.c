/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_element.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 20:32:42 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 11:20:54 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <math.h>
#include "elements.h"
#include "list.h"

static char			*g_str_type[_MAX_TYPE_ELEM] = {
	"Sphere",
	"Spot",
	"Plane",
	"Cylinder",
	"Cone",
	"Disk",
	"Blob"
};

static void			dump(t_element *self)
{
	ft_putstr("\033[7m");
	if (self->name)
		printf("====== %s: %s ======\n", g_str_type[self->type], self->name);
	else
		printf("====== %s ======\n", g_str_type[self->type]);
	ft_putstr("\033[0m");
	printf("Center\t\t\t\t%s\n", str_v3f(self->center));
	printf("Rotation\t\t\t%s\n", str_v3f(self->dir));
	printf("Size\t\t\t\t%s\n", str_v3f(self->size));
	printf("Ambiant_color\t\t\t%s\n", str_color(self->ambiante_color));
	printf("Diffuse_color\t\t\t%s\n", str_color(self->diffuse_color));
	printf("Specular_color\t\t\t%s\n", str_color(self->specular_color));
	if (self->reflection)
		printf("Reflection:\t\t\t%lu\n", self->reflection);
	if (self->refraction != 1.)
		printf("Refraction:\t\t\t%f\n", self->refraction);
	if (self->transparency != 0)
		printf("Transparency:\t\t\t%zu\n", self->transparency);
	if (self->texture)
		printf("Texture:\t\t\t%s\n", self->texture->name);
	if (self->texture)
		printf("Texture size:\t\t\t%s\n", str_v3f(self->texture_size));
	if (self->bump)
		printf("Bump Mapping:\t\t\t%s\n", self->bump->name);
	ft_putchar('\n');
}

static t_color		get_color(t_element *self, t_vect3f inter)
{
	t_vect3f			uv;
	t_vect3f			normale;

	if (!self->texture)
		return (self->ambiante_color);
	normale = rotate(self->normale(self, inter), self->dir);
	uv = self->uv_mapping(self, inter, normale, 0);
	return (mlx_get_value(&self->texture->img, uv.x, uv.y));
}

static t_vect3f		bump_mapping(t_element *self,
						t_vect3f normale, t_vect3f inter)
{
	t_vect3f			xy;
	t_vect3f			noise;
	t_color				noise_c;

	if (!self->bump)
		return (normale);
	xy = self->uv_mapping(self, inter, rotate(normale, self->dir), 1);
	noise_c = mlx_get_value(&self->bump->img, xy.x, xy.y);
	noise.x = (float)(noise_c.rgba.r / 255.0) * 0.7;
	noise.y = (float)(noise_c.rgba.g / 255.0) * 0.7;
	noise.z = (float)(noise_c.rgba.b / 255.0) * 0.7;
	normale = sum_v3f(normale, noise);
	return (normale);
}

t_element			*new_element(enum e_type type)
{
	t_element		*new;

	new = NEW_LIST(t_element);
	if (!new)
		return (NULL);
	new->type = type;
	new->dump = &dump;
	new->get_color = &get_color;
	new->bump_mapping = &bump_mapping;
	new->perlin = 1;
	return (new);
}
