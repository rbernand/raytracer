/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_cone.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 12:44:40 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 04:01:38 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdlib.h>
#include <libvect.h>
#include <libft.h>
#include <rt.h>
#include <elements.h>

static t_vect3f		uv_mapping(t_element *self, t_vect3f inter,
						t_vect3f normale, char is_bump)
{
	(void)inter;
	(void)self;
	(void)normale;
	(void)is_bump;
	return (NULL_V3F);
}

static t_vect3f		normale(t_element *self, t_vect3f inter)
{
	t_vect3f				normale;

	normale = sub_v3f(inter, self->center);
	normale = rotate(normale, self->dir);
	normale.z = -(tan(self->size.x) * normale.z);
	normalize_v3f(&normale);
	return (rotate_neg(normale, self->dir));
}

static t_return		intersect(t_element *cone, t_cam cam, float *dist)
{
	float			abc[3];
	float			delta;
	float			x[2];

	abc[0] = pow(cam.dir.x, 2) + pow(cam.dir.y, 2)
		- tan(cone->size.x) * pow(cam.dir.z, 2);
	abc[1] = 2 * (cam.dir.x * cam.ori.x + cam.dir.y * cam.ori.y
			- tan(cone->size.x) * cam.dir.z * cam.ori.z);
	abc[2] = cam.ori.x * cam.ori.x + cam.ori.y * cam.ori.y
		- tan(cone->size.x) * cam.ori.z * cam.ori.z;
	if ((delta = abc[1] * abc[1] - 4 * abc[0] * abc[2]) < 0)
		return (_ERR);
	x[0] = (-abc[1] - sqrt(delta)) / (2 * abc[0]);
	x[1] = (-abc[1] + sqrt(delta)) / (2 * abc[0]);
	if (!((x[0] < x[1] || x[1] < 0) && x[0] > 0))
		return (_ERR);
	if (cone->size.z <= 0 || (fabs(cam.dir.z * x[0] + cam.ori.z) < cone->size.z
				&& cam.dir.z * x[0] + cam.ori.z < 0))
		*dist = x[0];
	else if (cone->size.z <= 0 || (fabs(cam.dir.z * x[1] + cam.ori.z)
				< cone->size.z && cam.dir.z * x[1] + cam.ori.z < 0))
		*dist = x[1];
	else
		return (_ERR);
	return (_SUCCESS);
}

t_element			*new_cone(t_vect3f center, t_vect3f dir,
					t_vect3f size, t_color color)
{
	t_element			*new;

	new = new_element(_CONE);
	new->center = center;
	new->dir = dir;
	new->size = size;
	new->ambiante_color = color;
	new->intersect = &intersect;
	new->normale = &normale;
	new->uv_mapping = NULL;
	(void)uv_mapping;
	return (new);
}
