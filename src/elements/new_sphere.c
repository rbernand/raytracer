/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_sphere.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmerlier <tmerlier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 12:44:40 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/06 19:05:35 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdlib.h>
#include <libvect.h>
#include <libft.h>
#include <rt.h>
#include <elements.h>

static t_vect3f		uv_mapping(t_element *self, t_vect3f inter,
						t_vect3f normale, char is_bump)
{
	float					u;
	float					v;
	t_vect3f				xy;
	t_texture				*texture;

	(void)inter;
	texture = is_bump ? self->bump : self->texture;
	u = 0.5 + atan2(normale.y, normale.x) / (2 * M_PI);
	v = 0.5 - asinf(normale.z) / M_PI;
	xy.x = (int)(texture->img.size_xy[0] * (u - (int)(u)));
	xy.y = (int)(texture->img.size_xy[1] * (v - (int)(v)));
	return (xy);
}

static t_vect3f		normale(t_element *self, t_vect3f inter)
{
	t_vect3f		normale;

	normale = sub_v3f(inter, self->center);
	normalize_v3f(&normale);
	return (normalize_v3f(&normale));
}

static t_return		intersect(t_element *sphere, t_cam cam, float *dist)
{
	float			abc[3];
	float			delta;
	float			x[2];

	abc[0] = dot_product_v3f(cam.dir, cam.dir);
	abc[1] = 2 * dot_product_v3f(cam.dir, cam.ori);
	abc[2] = dot_product_v3f(cam.ori, cam.ori) - sphere->size.x
		* sphere->size.x;
	delta = abc[1] * abc[1] - 4 * abc[0] * abc[2];
	if (delta < 0)
		return (_ERR);
	x[0] = (-abc[1] - sqrt(delta)) / (2 * abc[0]);
	x[1] = (-abc[1] + sqrt(delta)) / (2 * abc[0]);
	if ((x[0] < x[1] || x[1] < 0) && x[0] > 0)
		*dist = x[0];
	else
		*dist = x[1];
	return (_SUCCESS);
}

t_element			*new_sphere(t_vect3f center, t_vect3f dir,
					t_vect3f size, t_color color)
{
	t_element			*new;

	new = new_element(_SPHERE);
	new->center = center;
	new->dir = dir;
	new->size = size;
	new->ambiante_color = color;
	new->intersect = &intersect;
	new->normale = &normale;
	new->uv_mapping = &uv_mapping;
	return (new);
}
