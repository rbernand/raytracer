/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_cube.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 09:38:57 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 11:22:20 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <stdlib.h>
#include "libvect.h"
#include "libft.h"
#include "rt.h"
#include "elements.h"

static t_vect3f		uv_mapping(t_element *self, t_vect3f inter,
						t_vect3f normale, char is_bump)
{
	float					u;
	float					v;
	t_vect3f				xy;
	t_texture				*texture;

	(void)inter;
	texture = is_bump ? self->bump : self->texture;
	u = 0.5 + atan2(normale.y, normale.x) / (2 * M_PI);
	v = 0.5 - asinf(normale.z) / M_PI;
	xy.x = (int)(texture->img.size_xy[0] * (u - (int)(u)));
	xy.y = (int)(texture->img.size_xy[1] * (v - (int)(v)));
	return (xy);
}

static t_vect3f		normale(t_element *self, t_vect3f inter)
{
	t_vect3f		normale;

	normale = sub_v3f(inter, self->center);
	normalize_v3f(&normale);
	return (normalize_v3f(&normale));
}

static t_return		intersect(t_element *self, t_cam cam, float *dist)
{
	t_element			*blob;
	float				tmp_dist;
	float				tnear;
	t_cam				current_cam;

	tnear = INFINITY;
	blob = self->blobs;
	while (blob)
	{
		current_cam = reposition(cam, blob);
		if (blob->intersect(blob, current_cam, &tmp_dist) == _SUCCESS
				&& (tmp_dist > 0 && tmp_dist < tnear && (tnear = tmp_dist)))
		{
			*dist = tmp_dist;
		}
		blob = blob->next;
	}
	return (tnear != INFINITY && tnear > 0 ? _SUCCESS : _ERR);
}

static void			dump(t_element *self)
{
	ft_putstr("\033[7m");
	printf("====== %s ======\n", "Blob");
	ft_putstr("\033[0m");
	blobs = self->blobs;
	while (blobs)
	{
		printf("Centers\t\t\t\t%s\n", str_v3f(blobs->center));
		printf("Size\t\t\t\t%s\n", str_v3f(blobs->size));
		blobs = blobs->next;
	}
	printf("Ambiant_color\t\t\t%s\n", str_color(self->ambiante_color));
	printf("Diffuse_color\t\t\t%s\n", str_color(self->diffuse_color));
	printf("Specular_color\t\t\t%s\n", str_color(self->specular_color));
	if (self->reflection)
		printf("Reflection:\t\t\t%lu\n", self->reflection);
	if (self->refraction != 1.)
		printf("Refraction:\t\t\t%f\n", self->refraction);
	if (self->transparency != 0)
		printf("Transparency:\t\t\t%zu\n", self->transparency);
	if (self->texture)
		printf("Texture:\t\t\t%s\n", self->texture->name);
	if (self->bump)
		printf("Bump Mapping:\t\t\t%s\n", self->bump->name);
	ft_putchar('\n');
}

t_element			*new_cube(t_vect3f center, t_vect3f dir,
					t_vect3f size, t_color color)
{
	t_element			*new;

	new = new_element(_BLOB);
	new->center = center;
	new->dir = dir;
	new->size = size;
	new->ambiante_color = color;
	new->intersect = &intersect;
	new->normale = &normale;
	new->uv_mapping = &uv_mapping;
	new->dump = &dump;
	return (new);
}
