/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_plane.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/29 18:36:03 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 08:07:18 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <libft.h>
#include <libvect.h>
#include <rt.h>

static t_vect3f		uv_mapping(t_element *self, t_vect3f inter,
						t_vect3f normale, char is_bump)
{
	t_vect3f			xy;
	t_texture			*texture;
	t_vect3f			dist;

	texture = is_bump ? self->bump : self->texture;
	(void)normale;
	inter = rotate(inter, self->dir);
	if (!self->size.x && !self->size.y)
	{
		xy.x = (int)(inter.x / self->texture_size.x) % texture->img.size_xy[0];
		xy.y = -(int)(inter.y / self->texture_size.y) % texture->img.size_xy[1];
	}
	else
	{
		dist = sub_v3f(inter, self->center);
		xy.x = (int)((0.5 - (dist.x / self->size.x / 2))
				* texture->img.size_xy[0]);
		xy.y = (int)((0.5 - (dist.y / self->size.y / 2))
				* texture->img.size_xy[1]);
	}
	if (xy.x < 0)
		xy.x += texture->img.size_xy[0];
	if (xy.y < 0)
		xy.y += texture->img.size_xy[1];
	return (xy);
}

static t_vect3f		normale(t_element *self, t_vect3f inter)
{
	static t_vect3f			normale = {0, 0, -1};

	(void)inter;
	return (rotate(normale, self->dir));
}

static t_return		intersect(t_element *self, t_cam cam, float *dist)
{
	float			x;
	float			tmp;

	if (cam.dir.z)
	{
		x = -cam.ori.z / cam.dir.z;
		if (self->size.x == 0 && self->size.y == 0)
		{
			*dist = x;
			return (_SUCCESS);
		}
		else
		{
			tmp = cam.dir.x * x + cam.ori.x;
			if (fabs(tmp) < self->size.x
					&& fabs(cam.dir.y * x + cam.ori.y) < self->size.y)
			{
				*dist = x;
				return (_SUCCESS);
			}
		}
	}
	return (_ERR);
}

t_element			*new_plane(t_vect3f center, t_vect3f dir, t_vect3f size,
					t_color color)
{
	t_element			*new;

	new = new_element(_PLANE);
	new->center = center;
	new->dir = dir;
	new->size = size;
	new->ambiante_color = color;
	new->intersect = &intersect;
	new->normale = &normale;
	new->uv_mapping = &uv_mapping;
	return (new);
}
