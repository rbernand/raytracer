/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sum_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/27 12:23:32 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/07 15:38:54 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mymlx.h>

#define MIN(X,Y)		((X) < (Y) ? (X) : (Y))
#define MAX(X,Y)		((X) > (Y) ? (X) : (Y))

t_color			sum_color(t_color a, t_color b)
{
	a.rgba.r = MIN(MAX(a.rgba.r + b.rgba.r, 0), 255);
	a.rgba.g = MIN(MAX(a.rgba.g + b.rgba.g, 0), 255);
	a.rgba.b = MIN(MAX(a.rgba.b + b.rgba.b, 0), 255);
	a.rgba.a = MIN(MAX(a.rgba.a + b.rgba.a, 0), 255);
	return (a);
}
