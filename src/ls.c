/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/02 10:32:36 by vcosson           #+#    #+#             */
/*   Updated: 2016/03/02 21:06:19 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <parser.h>
#include <prompt.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <dirent.h>

static void		read_repo(DIR *dp, char *path)
{
	struct dirent	*ep;
	struct stat		buf;
	char			*file_path;

	while ((ep = readdir(dp)) != NULL)
	{
		if ((ep->d_name)[0] != '.')
		{
			file_path = ft_strjoin(path, ep->d_name);
			stat(file_path, &buf);
			free(file_path);
			if (S_ISDIR(buf.st_mode))
				printf("%s%s%s\n", CYAN, ep->d_name, NORMAL);
			else
			{
				if (ft_strstr(ep->d_name, ".rt") != NULL)
					printf("%s%s%s\n", RED, ep->d_name, NORMAL);
				else
					printf("%s\n", ep->d_name);
			}
		}
	}
	closedir(dp);
}

t_return		ls(char **cmd)
{
	DIR				*dp;

	dp = (cmd[0] != NULL) ? opendir(cmd[0]) : opendir(".");
	if (dp == NULL)
		return (PERROR("opendir: cannot read directory"));
	if (cmd[0] != NULL)
		read_repo(dp, ft_strjoin(cmd[0], "/"));
	else
		read_repo(dp, "./");
	return (_SUCCESS);
}
