/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   random_semaphore.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/04 17:29:42 by etermeau          #+#    #+#             */
/*   Updated: 2015/10/14 19:49:09 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>

#define SEM_NAME_LEN 12

const char		*random_semaphore(void)
{
	char			*charset;
	static char		semaphore[SEM_NAME_LEN + 1];
	static int		i = 0;
	int				key;

	if (i == SEM_NAME_LEN)
		return (semaphore);
	ft_bzero(semaphore, SEM_NAME_LEN);
	charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	ft_strcpy(semaphore, "/tmp/");
	i = ft_strlen(semaphore);
	while (i < SEM_NAME_LEN)
	{
		key = rand() % ft_strlen(charset);
		semaphore[i] = charset[key];
		i++;
	}
	semaphore[SEM_NAME_LEN] = '\0';
	return (semaphore);
}
