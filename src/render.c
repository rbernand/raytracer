/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 12:43:42 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 12:21:03 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <mlx.h>
#include <semaphore.h>
#include <pthread.h>
#include <math.h>
#include <libft.h>
#include <elements.h>
#include <rt.h>
#include <sys/time.h>

#define INV(X)			(1. / (float)(X))
#define FOV				30
#define ANGLE			0.2679491924311227

static t_cam			init_cam(t_scene *scn, float x, float y,
							t_bool floating)
{
	t_cam			cam;
	float			aspect;

	aspect = (float)scn->win_x / (float)scn->win_y;
	cam.hitted = NULL;
	if (floating == _TRUE)
	{
		x += (random() % 10000) / 10000.0;
		y += (random() % 10000) / 10000.0;
	}
	cam.ori = scn->cam.ori;
	cam.dir.x = (2.0 * ((x + 0.5) / (float)scn->win_x) - 1) * (ANGLE * aspect);
	cam.dir.y = (1 - 2 * ((y + 0.5) / (float)scn->win_y)) * ANGLE;
	cam.dir.z = -1;
	normalize_v3f(&cam.dir);
	cam.dir = rotate(cam.dir, scn->cam.dir);
	normalize_v3f(&cam.dir);
	cam.depth = FILTER(0, scn->depth, 100);
	return (cam);
}

static t_color			multi_ray(t_scene *scn, int x, int y)
{
	ssize_t					i;
	ssize_t					j;
	int						rgba[4];
	t_color					tmp;

	i = -1;
	bzero(rgba, sizeof(int) * 4);
	while (++i < scn->sampling)
	{
		tmp = trace(init_cam(scn, (float)x, (float)y,
						scn->sampling == 1 ? _FALSE : _TRUE),
					scn->elems,
					MYMLX_BLACK);
		j = -1;
		while (++j < 4)
			rgba[j] += tmp.array[j];
	}
	j = -1;
	while (++j < 4)
		tmp.array[j] = rgba[j] / scn->sampling;
	tmp.rgba.r = FILTER(0, powf(tmp.rgba.r, scn->gamma), 255);
	tmp.rgba.g = FILTER(0, powf(tmp.rgba.g, scn->gamma), 255);
	tmp.rgba.b = FILTER(0, powf(tmp.rgba.b, scn->gamma), 255);
	return (tmp);
}

static void				*sub_render(t_scene *scn)
{
	static size_t		id;
	size_t				c_id;
	t_point				pixel;
	unsigned int		y_max;

	sem_wait(get_env(NULL)->thread_id);
	c_id = id++;
	sem_post(get_env(NULL)->thread_id);
	pixel.y = (scn->win_y / scn->threads) * c_id - 1;
	y_max = (scn->win_y / scn->threads) * (c_id + 1);
	while (++pixel.y < y_max)
	{
		pixel.x = -1;
		while (++pixel.x < scn->win_x)
		{
			pixel.color = multi_ray(scn, pixel.x, pixel.y);
			mlx_put_point(scn->imgs, &pixel);
		}
		scn->put_bar(scn);
	}
	id = c_id == scn->threads - 1 ? 0 : id;
	if (c_id >= 1)
		pthread_join(get_env(NULL)->threads[c_id - 1], NULL);
	pthread_exit(0);
	return (NULL);
}

static void				real_render(t_env *env, t_scene *scn)
{
	size_t				i;

	i = 0;
	while (i < scn->threads)
	{
		if (pthread_create(&env->threads[i], 0,
					(void *(*)(void *))&sub_render, scn) >= 0)
		{
			i++;
			continue ;
		}
		PERROR("pthread_create: failed to launch thread");
		break ;
	}
	if (pthread_join(env->threads[scn->threads - 1], NULL) < 0)
		PERROR("PTHREADJOIN");
}

void					render(t_env *env, t_scene *scn)
{
	struct timeval	start;
	struct timeval	end;

	env->threads = (pthread_t *)malloc(sizeof(pthread_t) * scn->threads);
	gettimeofday(&start, NULL);
	ft_putstr("\033[1mRendering: \033[0m");
	ft_putendl(scn->name);
	real_render(env, scn);
	gettimeofday(&end, NULL);
	printf("\n\033[1mDone in %f seconds.\033[0m\n",
			((end.tv_sec + end.tv_usec / 1000000.0)
			- (start.tv_sec + start.tv_usec / 1000000.0)));
	free(env->threads);
}
