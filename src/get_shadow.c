/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_shadow.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/12 12:05:26 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 12:17:16 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <elements.h>

#include <math.h>
#include <stdio.h>

#define SHADOW_RATIO			0.5

t_color				get_shadow(t_element *elements, t_element *elem,
						t_element *spot, t_vect3f inter)
{
	float			dist_spot;
	float			dist;
	t_cam			cam;
	t_color			shadow;

	cam.dir = sub_v3f(spot->center, inter);
	cam.ori = inter;
	dist_spot = get_norme_v3f(cam.dir);
	normalize_v3f(&cam.dir);
	shadow.rgba.a = 0;
	while (elements)
	{
		if (elements != elem && elements->type != _SPOT)
		{
			if (elements->intersect(elements, reposition(cam, elements), &dist)
				== _SUCCESS && dist > 0 && dist < dist_spot)
			{
				shadow.rgba.a = MAX(shadow.rgba.a,
					(100 - elements->transparency));
			}
		}
		elements = elements->next;
	}
	return (shadow);
}
