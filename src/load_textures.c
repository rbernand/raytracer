/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_texture.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/08 18:24:46 by rbernand          #+#    #+#             */
/*   Updated: 2015/09/22 16:35:02 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <libft.h>
#include <mymlx.h>
#include <rt.h>

t_return		load_texture(t_mlx *mlx, t_texture *texture)
{
	if ((texture->img.ptr = mlx_xpm_file_to_image(mlx->ptr,
			texture->input_file,
			&texture->img.size_xy[0],
			&texture->img.size_xy[1])) == NULL)
		return (PERROR("mlx_xpm_file_to_image: Cannont create image"));
	if ((texture->img.data = mlx_get_data_addr(texture->img.ptr,
				&texture->img.bp,
				&texture->img.size_line, &texture->img.endian)) == 0)
		return (PERROR("mlx_new_image: Cannont create image."));
	return (_SUCCESS);
}
