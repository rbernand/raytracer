/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/09 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2015/08/27 11:51:31 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <mymlx.h>

char		*str_color(t_color c)
{
	static char		buf[41];

	sprintf(buf, "\033[41m %3d \033[42m %3d \033[44m %3d \033[0m %d%%",
				c.rgba.r, c.rgba.g, c.rgba.b, c.rgba.a);
	return (buf);
}
