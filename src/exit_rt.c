/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit_rt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/09 08:32:01 by rbernand          #+#    #+#             */
/*   Updated: 2015/10/09 15:56:22 by vcosson          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <rt.h>

static t_return		destroy_semaphore(sem_t *sem)
{
	if (sem_close(sem) != 0)
		return (PERROR("sem_close: cannont close semaphore."));
	if (sem_unlink(random_semaphore()) != 0)
		return (PERROR("sem_unlink: cannont unlink semaphore."));
	return (_SUCCESS);
}

void				exit_rt(void)
{
	t_env			*env;

	env = get_env(NULL);
	if (destroy_semaphore(env->thread_id))
		exit(PERROR("destroy_semaphore: cannot close semaphore properly."));
	exit(0);
}
