/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reposition.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/02 10:32:41 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/02 21:04:39 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libvect.h>
#include <elements.h>
#include <rt.h>

t_cam				reposition(t_cam cam, t_element *ref)
{
	t_cam			new;

	new.ori = sub_v3f(cam.ori, ref->center);
	new.dir = rotate(cam.dir, ref->dir);
	new.ori = rotate(new.ori, ref->dir);
	return (new);
}
