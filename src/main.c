/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 12:12:58 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 07:15:00 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <semaphore.h>
#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <fcntl.h>
#include "mymlx.h"
#include "libft.h"
#include "mlx.h"
#include "elements.h"
#include "parser.h"
#include "rt.h"
#include "prompt.h"
#include "getopt42.h"

static t_return		create_semaphore(sem_t **sem)
{
	sem_t			*tmp;

	if ((tmp = sem_open(random_semaphore(), O_CREAT | O_EXCL, S_IRWXU, 1))
			== SEM_FAILED)
		return (PERROR("sem_open: Failed to create semaphore."));
	*sem = tmp;
	return (_SUCCESS);
}

static t_return		init_opt(t_env *env, int *argc, char ***argv)
{
	set_opt42('i', "interactive", 0, &env->interactive);
	set_opt42('v', "interactive", 0, &env->verbose);
	parse_opt42(*argc, *argv);
	shift_arg(argc, argv);
	return (_SUCCESS);
}

static t_return		global_init(t_env *env, t_mlx *mlx)
{
	srand(time(NULL));
	if (create_semaphore(&env->thread_id) == _ERR)
		return (PERROR("create_semaphore: Failed to init semaphore."));
	if ((mlx->ptr = mlx_init()) == 0)
		return (PERROR("mlx_init: Failed to load mlx."));
	set_signal_handler();
	return (_SUCCESS);
}

int					load_args(char **args)
{
	static char		*sub_cmd[2] = {NULL, NULL};
	int				counter;

	counter = 0;
	while (*args)
	{
		sub_cmd[0] = *args;
		if (make(sub_cmd) == _SUCCESS)
			counter++;
		args++;
		mlx_do_sync(get_mlx(NULL)->ptr);
	}
	return (counter);
}

int					main(int ac, char **av)
{
	t_env			env;

	(void)ac;
	get_env(&env);
	bzero(&env, sizeof(t_env));
	get_mlx(&env.mlx);
	ft_bzero(&env, sizeof(t_env));
	init_opt(&env, &ac, &av);
	if (global_init(&env, &env.mlx) == _ERR)
		return (PERROR("global_init: Cannont start RT."));
	if (!load_args(av + 1))
		return (PERROR("No valid scene."));
	if (env.interactive == 1)
		prompt();
	else if (ac > 1)
		mlx_loop(env.mlx.ptr);
	return (0);
}
