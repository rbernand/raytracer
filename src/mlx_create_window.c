/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_create_window.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/26 21:38:37 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 07:04:45 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "mymlx.h"
#include "mlx.h"
#include "rt.h"
#include "rt.h"
#include "list.h"
#include "prompt.h"
#include "filter.h"

static void					my_image_destroy(t_img *img)
{
	mlx_destroy_image(get_mlx(NULL)->ptr, img->ptr);
	free(img);
}

static void					delete_window(t_window *win)
{
	t_scene				*scn;

	scn = win->parent_scn;
	while (scn->imgs)
		LIST_DELETE(&scn->imgs, scn->imgs,
				(void (*)(void *))my_image_destroy);
	mlx_destroy_window(get_mlx(NULL)->ptr, win->ptr);
	exit_rt();
}

static int					key_hook(int key, t_scene *scn)
{
	t_mlx				*mlx;
	char				redraw;

	redraw = 0;
	mlx = get_mlx(NULL);
	if (key == KEY_ESC)
	{
		LIST_DELETE(&mlx->windows, &scn->win,
				(void (*)(void *))&delete_window);
		if (!mlx->windows)
			exit_rt();
	}
	else if (key == 1 && (redraw = 1))
		save((char*[]){ft_itoa(scn->id, 10), scn->name});
	else if (key >= 18 && key <= 18 + _MAX_FILTER && (redraw = 1))
		filter((char *[]){ft_itoa(scn->id, 10),
				(char *)g_filter_name[key - 18]});
	else if (key == 123 && (redraw = 1))
		LIST_ROTATE_R(&scn->imgs);
	else if (key == 124 && (redraw = 1))
		LIST_ROTATE(&scn->imgs);
	if (redraw)
		mlx_display_result(get_mlx(NULL), scn);
	return (0);
}

t_return					mlx_create_window(t_scene *scn)
{
	static unsigned int		id = 0;
	t_mlx					*mlx;

	mlx = get_mlx(NULL);
	if (scn->win.ptr != NULL)
		return (PERROR("Window already existing."));
	if (!(scn->win.ptr = mlx_new_window(get_mlx(NULL)->ptr, scn->win_x,
			scn->win_y, scn->name)))
		return (PERROR("mlx_new_window: Unable to open window."));
	PUSH_BACK(&mlx->windows, &scn->win);
	scn->win.id = id++;
	scn->win.parent_scn = scn;
	mlx_hook(scn->win.ptr, 17, 0, (int (*)(void *))&delete_window, &scn->win);
	mlx_key_hook(scn->win.ptr, &key_hook, scn);
	return (_SUCCESS);
}
