/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_display_result.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/21 10:04:26 by rbernand          #+#    #+#             */
/*   Updated: 2016/02/21 10:06:29 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "mymlx.h"
#include "rt.h"

t_return			mlx_display_result(t_mlx *mlx, t_scene *scn)
{
	mlx_put_image_to_window(mlx->ptr, scn->win.ptr, scn->imgs->ptr, 0, 0);
	return (_SUCCESS);
}
