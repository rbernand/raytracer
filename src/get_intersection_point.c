/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_intersection_point.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbenand@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/01 11:50:47 by rbernand          #+#    #+#             */
/*   Updated: 2015/10/01 12:43:56 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libvect.h>
#include <rt.h>

t_vect3f			get_intersection_point(t_cam cam, float dist)
{
	return (sum_v3f(cam.ori, prod_v3f(cam.dir, dist)));
}
