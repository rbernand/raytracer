/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   min_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 17:27:45 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/07 17:28:12 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mymlx.h>

#define MIN(X,Y)		((X) < (Y) ? (X) : (Y))

t_color					min_color(t_color a, t_color b)
{
	ssize_t				i;
	t_color				out;

	i = -1;
	while (++i < 4)
		out.array[i] = MIN(a.array[i], b.array[i]);
	return (out);
}
