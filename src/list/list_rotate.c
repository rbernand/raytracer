/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_rotate.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 12:26:01 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/07 13:11:02 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void			list_rotate(t_list **lst)
{
	t_list			*tmp;
	t_list			*back;

	back = LIST_BACK(*lst);
	if (back == *lst)
		return ;
	tmp = *lst;
	while (tmp && tmp->next != back)
		tmp = tmp->next;
	tmp->next = 0;
	tmp = *lst;
	back->next = *lst;
	*lst = back;
}
