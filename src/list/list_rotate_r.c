/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_rotate.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbernand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 12:26:01 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/07 13:11:16 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void			list_rotate_r(t_list **lst)
{
	t_list			*tmp;
	t_list			*next;
	t_list			*back;

	back = LIST_BACK(*lst);
	if (back == *lst)
		return ;
	next = (*lst)->next;
	tmp = *lst;
	tmp->next = 0;
	back->next = tmp;
	*lst = next;
}
