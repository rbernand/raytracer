/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit_wrapper.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbenand@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/10 15:15:14 by rbernand          #+#    #+#             */
/*   Updated: 2015/09/24 12:07:13 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <rt.h>

t_return			exit_wrapper(char **cmd)
{
	(void)cmd;
	exit_rt();
	return (_SUCCESS);
}
