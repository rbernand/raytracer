/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   singleton.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/09 11:57:32 by rbernand          #+#    #+#             */
/*   Updated: 2015/10/05 09:26:56 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mymlx.h>
#include <rt.h>

t_env			*get_env(t_env *env)
{
	static t_env			*memory = 0;

	if (env)
		memory = env;
	return (memory);
}

t_scene			*current_scene(t_scene *scn)
{
	static t_scene			*memory = 0;

	if (scn == (void *)-1)
		memory = 0;
	else
		memory = scn;
	return (memory);
}

t_mlx			*get_mlx(t_mlx *mlx)
{
	static t_mlx			*memory = 0;

	if (mlx)
		memory = mlx;
	return (memory);
}
