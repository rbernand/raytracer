/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trace.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/05 14:59:47 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 12:24:21 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <math.h>
#include "libvect.h"
#include <rt.h>
#include <elements.h>

static t_color			apply_reflection(t_cam hit, t_cam cam,
							t_element *elements, t_color color)
{
	t_color				result;
	t_vect3f			normale;

	hit.depth = cam.depth;
	normale = hit.hitted->normale(hit.hitted, hit.ori);
	hit.dir = sub_v3f(cam.dir,
						prod_v3f(normale,
							2.0 * dot_product_v3f(cam.dir, normale)));
	if (hit.hitted->bump)
		hit.dir = hit.hitted->bump_mapping(hit.hitted, hit.dir, hit.ori);
	result = trace(hit, elements, color);
	result = mix_color(color, result, 1.0 - hit.hitted->reflection / 100.0,
				hit.hitted->reflection / 100.0);
	return (result);
}

static t_color			apply_refraction(t_cam hit, t_cam cam,
							t_element *elements, t_color color)
{
	t_color				result;
	t_vect3f			normale;
	float				n;
	float				cos1;

	hit.depth = cam.depth;
	normale = hit.hitted->normale(hit.hitted, hit.ori);
	n = cam.hitted == 0 ? 1 : cam.hitted->refraction / hit.hitted->refraction;
	cos1 = dot_product_v3f(cam.dir, normale);
	if (cos1 > 0)
		hit.dir = cam.dir;
	else
		hit.dir = sub_v3f(prod_v3f(cam.dir, n),
		prod_v3f(normale, (n * cos1
				+ sqrt(1 - n * n * (1 - cos1 * cos1)))));
	result = trace(hit, elements, result);
	return (mix_color(color, result, hit.hitted->transparency / 100.0,
			hit.hitted->transparency / 100.0));
}

static t_color			apply_transparency(t_cam hit, t_cam cam,
							t_element *elements, t_color color)
{
	t_color				result;

	hit.depth = cam.depth;
	hit.dir = cam.dir;
	result = trace(hit, elements, color);
	return (mix_color(color, result, hit.hitted->transparency / 100.0,
		hit.hitted->transparency / 100.0));
}

static t_cam			find_intersection(t_element *elements, t_cam cam)
{
	t_cam				out;
	t_cam				current_cam;
	float				tnear;
	float				dist;

	tnear = INFINITY;
	bzero(&out, sizeof(out));
	while (elements)
	{
		if (elements != cam.hitted && elements->type != _SPOT)
		{
			current_cam = reposition(cam, elements);
			if (elements->intersect(elements, current_cam, &dist) == _SUCCESS
					&& (dist > 0 && dist < tnear) && (tnear = dist))
				out.hitted = elements;
		}
		elements = elements->next;
	}
	if (out.hitted)
		out.ori = get_intersection_point(cam, tnear);
	return (out);
}

t_color					trace(t_cam cam, t_element *elements, t_color color)
{
	t_cam				elem_hit;
	t_color				result;

	if (cam.depth == 0)
		return (color);
	cam.depth = cam.depth - 1;
	if (!(elem_hit = find_intersection(elements, cam)).hitted)
		return (MYMLX_BLACK);
	result = get_color(elem_hit.hitted, elements, elem_hit.ori);
	if (elem_hit.hitted->reflection != 0)
		result = apply_reflection(elem_hit, cam, elements, result);
	if (elem_hit.hitted->refraction != 1.0)
		result = apply_refraction(elem_hit, cam, elements, result);
	else if (elem_hit.hitted->transparency)
		result = apply_transparency(elem_hit, cam, elements, result);
	return (result);
}
