/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbenand@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/09 09:02:27 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 06:54:13 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt.h>
#include <signal.h>

static void			exit_handler(int sig)
{
	if (sig == SIGINT && get_env(NULL)->interactive)
		ft_putendl("Please use 'exit' command.");
	if (sig == SIGSEGV)
		ft_putendl("\033[733mKilled By SEGV\033[0m");
	if (sig == SIGBUS)
		ft_putendl("\033[733mError in scene. Too many reflexion or " \
				"item in the same place\033[0m");
	exit_rt();
}

void				set_signal_handler(void)
{
	signal(SIGINT, &exit_handler);
	signal(SIGSEGV, &exit_handler);
}
