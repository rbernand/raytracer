/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mix_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/05 09:49:08 by rbernand          #+#    #+#             */
/*   Updated: 2015/09/05 09:51:02 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mymlx.h>

t_color			mix_color(t_color dst, t_color src, float a, float b)
{
	t_color			out;

	out = sum_color(prod_color(dst, 1. - a),
			prod_color(src, b));
	return (out);
}
