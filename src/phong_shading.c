/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phong_shading.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/05 18:44:02 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 07:56:09 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <math.h>
#include <mymlx.h>
#include <rt.h>

#define MAX(X,Y)		((X) > (Y) ? (X) : (Y))
#define MIN(X,Y)		((X) < (Y) ? (X) : (Y))

static float		diffuse_color(t_vect3f light, t_vect3f normale)
{
	float				n2;

	n2 = dot_product_v3f(normale, normale) * dot_product_v3f(light, light);
	return (dot_product_v3f(normale, light) * (1 / sqrt(n2)));
}

static float		specular_color(t_vect3f light, t_vect3f normale)
{
	t_vect3f		ref;

	ref = sub_v3f(light,
			prod_v3f(normale, 2 * dot_product_v3f(light, normale)));
	return (-(dot_product_v3f(normale, ref) / get_norme_v3f(ref)));
}

static t_color		get_effect(float d_s[2], t_color spot, t_element *elem)
{
	int					i;
	unsigned int		k;
	t_color				out;

	i = -1;
	while (++i < 3)
	{
		k = (elem->diffuse_color.array[i] * d_s[0]
				+ elem->specular_color.array[i] * d_s[1])
			* (spot.rgba.a / 100.0)
			* (spot.array[i] / 255.0);
		out.array[i] = MIN(MAX(k, 0), 255);
	}
	return (out);
}

t_color				phong_shading(t_element *elem, t_element *spot,
					t_vect3f inter, t_color base_color)
{
	t_vect3f		light;
	float			d_s[2];
	t_vect3f		normale;

	light = sub_v3f(spot->center, inter);
	normalize_v3f(&light);
	normale = elem->normale(elem, inter);
	normale = elem->bump_mapping(elem, normale, inter);
	d_s[0] = fmax(diffuse_color(light, normale), 0)
			* elem->diffuse_color.rgba.a / 100;
	d_s[1] = fmax(pow(specular_color(light, normale), 80)
			* elem->specular_color.rgba.a / 100, 0);
	return (sum_color(base_color,
				get_effect(d_s, spot->emission_color, elem)));
}
