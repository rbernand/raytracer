/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/23 18:51:53 by rbernand          #+#    #+#             */
/*   Updated: 2016/02/04 13:25:14 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include <stdio.h>
#include <libft.h>
#include <rt.h>
#include <unistd.h>
#include <mlx.h>
#include <prompt.h>

static const char		*g_lst_cmd[_NB_COMMANDS] = {
	"exit",
	"help",
	"cd",
	"make",
	"save",
	"list",
	"ls",
	"close",
	"filter"
};

static t_return			help(char **cmd)
{
	size_t			i;

	(void)cmd;
	i = 0;
	while (i < _NB_COMMANDS)
	{
		ft_putstr(g_lst_cmd[i++]);
		if (i != _NB_COMMANDS)
			ft_putchar(' ');
	}
	ft_putchar('\n');
	return (_SUCCESS);
}

static t_command		get_command(char *cmd)
{
	int					i;

	i = -1;
	while (++i < _NB_COMMANDS)
		if (ft_strncmp(cmd, g_lst_cmd[i], ft_strlen(g_lst_cmd[i])) == 0)
			return (i);
	printf("Command not found\n");
	return (_CMD_ERROR);
}

static t_return			cd(char **cmd)
{
	if (!cmd[0])
		return (PERROR("cd: missing path"));
	if (chdir(cmd[0]) < 0)
		return (PERROR("cd: Cannot access to path"));
	return (_SUCCESS);
}

static t_return			exec_cmd(t_command cmd, char *line)
{
	char					**splitted;
	t_return				ret;
	t_exec_cmd				done;
	static t_exec_cmd		cmds[_NB_COMMANDS] = {
	&exit_wrapper,
	&help,
	&cd,
	&make,
	&save,
	&list_scenes,
	&ls,
	NULL,
	&filter
	};

	splitted = ft_strsplit(line, ' ');
	done = cmds[cmd];
	ret = done(splitted + 1);
	ft_tabdel(&splitted);
	return (ret);
}

t_return				prompt(void)
{
	char		*line;
	t_command	cmd;

	line = NULL;
	while (ft_putstr("\033[34mRaytracer >>> \033[0m")
		&& get_next_line(0, &line))
	{
		cmd = get_command(line);
		if (cmd != _CMD_ERROR)
			exec_cmd(cmd, line);
		ft_strdel(&line);
	}
	exit_rt();
	return (_SUCCESS);
}
