/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   max_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbenand@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/01 12:39:08 by rbernand          #+#    #+#             */
/*   Updated: 2015/10/14 19:43:26 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mymlx.h>

#define MAX(X,Y)		((X) > (Y) ? (X) : (Y))

t_color					max_color(t_color a, t_color b)
{
	ssize_t				i;
	t_color				out;

	i = -1;
	while (++i < 4)
		out.array[i] = MAX(a.array[i], b.array[i]);
	return (out);
}
