/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prod_color.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/27 12:14:27 by rbernand          #+#    #+#             */
/*   Updated: 2015/08/27 21:19:03 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mymlx.h>

t_color			prod_color(t_color color, float ratio)
{
	t_color			ret;

	ret.rgba.r = color.rgba.r * ratio;
	ret.rgba.g = color.rgba.g * ratio;
	ret.rgba.b = color.rgba.b * ratio;
	ret.rgba.a = color.rgba.a * ratio;
	return (ret);
}
