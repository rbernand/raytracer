/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_put_point.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 20:58:02 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/05 15:03:16 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <mymlx.h>

const t_color			g_black = {.rgba = {0, 0, 0, 0}};
const t_color			g_white = {.rgba = {255, 255, 255, 0}};
const t_color			g_red = {.rgba = {0, 0, 255, 0}};
const t_color			g_green = {.rgba = {0, 255, 0, 0}};
const t_color			g_blue = {.rgba = {255, 0, 0, 0}};

t_color					mlx_get_value(const t_img *img, int x, int y)
{
	t_color				color;

	if (img->endian == 0)
		memcpy(&color.hexa, img->data + y * img->size_line + x * (img->bp / 8),
			sizeof(int));
	else
		memcpy(&color.hexa, img->data + y * img->size_line + x * (img->bp / 8),
			sizeof(int));
	return (color);
}

void					mlx_put_point(const t_img *img, const t_point *p)
{
	int			y;

	y = p->y * img->size_line;
	if (img->endian == 0)
		memcpy(img->data + y + p->x * (img->bp / 8),
			&p->color.hexa, sizeof(int));
	else
	{
	}
}
