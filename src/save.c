/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   save.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/02 15:09:09 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/07 14:05:24 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <libft.h>
#include <fcntl.h>
#include "rt.h"
#include "mymlx.h"
#include "prompt.h"

static void			write_header(int fd, t_scene *scn)
{
	ft_putchar_fd(0, fd);
	ft_putchar_fd(0, fd);
	ft_putchar_fd(2, fd);
	ft_putchar_fd(0, fd);
	ft_putchar_fd(0, fd);
	ft_putchar_fd(0, fd);
	ft_putchar_fd(0, fd);
	ft_putchar_fd(0, fd);
	ft_putchar_fd(0, fd);
	ft_putchar_fd(0, fd);
	ft_putchar_fd(0, fd);
	ft_putchar_fd(0, fd);
	ft_putchar_fd((scn->win_x & 0x00FF), fd);
	ft_putchar_fd((scn->win_x & 0xFF00) / 256, fd);
	ft_putchar_fd((scn->win_y & 0x00FF), fd);
	ft_putchar_fd((scn->win_y & 0xFF00) / 256, fd);
	ft_putchar_fd(scn->imgs->bp, fd);
	ft_putchar_fd(0, fd);
}

static t_return		write_img(int fd, t_scene *scn)
{
	t_img			*img;
	size_t			i;
	int				ret;

	img = scn->imgs;
	i = 0;
	while (i < scn->win_y)
	{
		ret = write(fd,
				img->data + img->size_line * (scn->win_y - ++i),
				img->size_line);
		if (ret < 0)
			return (_ERR);
	}
	return (_SUCCESS);
}

static char			*get_name(const char *scn_name)
{
	char			*name;
	int				i;

	name = ft_strjoin(scn_name, ".tga");
	i = -1;
	while (name[++i])
		if (name[i] == ' ')
			name[i] = '_';
	return (name);
}

t_return			save(char **args)
{
	int			fd;
	t_scene		*tmp;
	char		*name;

	if (args[0] == NULL || args[1] == NULL)
		return (PERROR("Missing Arguments"));
	name = get_name(args[1]);
	if ((fd = open(name, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR)) < 0)
		return (PERROR("open: Canont open dest file for saving."));
	if (!(tmp = get_scenes_by_id(ft_atoi(args[0]))))
		return (PERROR("get_scene_by_id: id invalid"));
	write_header(fd, tmp);
	if (write_img(fd, tmp) == _ERR)
		return (PERROR("write_img: rrror"));
	close(fd);
	free(name);
	return (_SUCCESS);
}
