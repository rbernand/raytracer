/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_create_image.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbernand <rbenand@student.42.fr>           +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/09 10:59:26 by rbernand          #+#    #+#             */
/*   Updated: 2015/10/01 12:45:42 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <mlx.h>
#include <string.h>
#include <libft.h>
#include <rt.h>
#include <mymlx.h>

static void		add_front_img(t_scene *scn, t_img *new_img)
{
	t_img	*tmp;

	tmp = scn->imgs;
	scn->imgs = new_img;
	new_img->next = tmp;
}

t_return		mlx_create_image(t_mlx *mlx, t_scene *scene)
{
	t_img			*img;

	img = (t_img *)malloc(sizeof(t_img));
	bzero(img, sizeof(t_img));
	if ((img->ptr = mlx_new_image(mlx->ptr, scene->win_x, scene->win_y)) == 0)
	{
		PERROR("mlx_new_image: Cannont create image.");
		free(img);
		return (_ERR);
	}
	if ((img->data = mlx_get_data_addr(img->ptr,
				&img->bp,
				&img->size_line,
				&img->endian)) == 0)
	{
		PERROR("mlx_new_image: Cannont create image.");
		mlx_destroy_image(mlx->ptr, img->ptr);
		free(img);
		return (_ERR);
	}
	bzero(img->data, sizeof(char) * img->size_line * scene->win_y);
	add_front_img(scene, img);
	return (_SUCCESS);
}
