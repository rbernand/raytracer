# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/02/24 06:29:29 by rbernand          #+#    #+#              #
#    Updated: 2016/03/16 13:38:20 by vcosson          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = rt
CC = cc
FLAGS = -Wall -Wextra -Werror -g -O3
LIBFT = libft/
LIBVECT = libvect/
LIBMLX11 = minilibx/
LIBMLXOSX = minilibxosx/
LIBOPT = libopt/
INCLUDES = include/
DIROBJ = obj/
DIRLIB = lib/
DIRSRC = src/
DIRBIN = bin/
DIRPARSER = parser/
DIRELEM = elements/
DIRLIST = list/
SRC = \
		main.c \
		exit_rt.c \
		signal.c \
		singleton.c \
		mlx_put_point.c \
		mlx_create_window.c \
		mlx_create_image.c \
		mlx_display_result.c \
		render.c \
		trace.c \
		get_color.c \
		get_shadow.c \
		phong_shading.c \
		load_textures.c \
		str_color.c \
		sum_color.c \
		prod_color.c \
		mix_color.c \
		max_color.c \
		reposition.c \
		get_intersection_point.c \
		prompt.c \
		save.c \
		make.c \
		exit_wrapper.c \
		list_scenes.c \
		ls.c \
		filter.c \
		apply_filter.c \
		random_semaphore.c \
		perlin.c \
		$(addprefix $(DIRELEM), \
			new_cone.c \
			new_element.c \
			new_sphere.c \
			new_spot.c \
			new_disk.c \
			new_plane.c \
			new_blob.c \
			new_cylinder.c) \
		$(addprefix $(DIRPARSER), \
			create_scene.c \
			load_scene.c \
			convert_parser.c \
			link_elements.c \
			new_elements_from_parser.c \
			parse_elem.c \
			center_factory.c \
			size_factory.c \
			radius_factory.c \
			color_factory.c \
			ambiante_color_factory.c \
			diffuse_color_factory.c \
			specular_color_factory.c \
			bump_factory.c \
			direction_factory.c \
			vecteur_factory.c \
			name_factory.c \
			texture_factory.c \
			texture_size_factory.c \
			file_factory.c \
			reflection_factory.c \
			refraction_factory.c \
			transparency_factory.c \
			get_scene_by_id.c \
			sampling_factory.c \
			depth_factory.c \
			transparency_factory.c \
			threads_factory.c \
			gamma_factory.c) \
		$(addprefix $(DIRLIST), \
			list_back.c \
			list_delete.c \
			list_new.c \
			list_count.c \
			list_iter.c \
			list_push_back.c \
			list_push_front.c \
			list_rotate.c \
			list_rotate_r.c)

OBJ=$(SRC:%.c=$(DIROBJ)%.o)

ARCHIVE_TEXTURE_NAME=texture_pack_rbernand_rt.tar

OS=$(shell uname -s)
ifeq ($(OS), Darwin)
	MLXFLAGS = -framework openGL -framework Appkit
endif
ifeq ($(OS), Linux)
	MLXFLAGS = -lX11 -lXext -lbsd -lm
endif
ifeq ($(OS), Darwin)
	LIBMLX=$(LIBMLXOSX)
endif
ifeq ($(OS), Linux)
	LIBMLX=$(LIBMLX11)
endif

all: init $(NAME) end

init:
	@mkdir -p $(DIRBIN)
	@mkdir -p $(DIRLIB)
	@mkdir -p $(DIROBJ)
	@mkdir -p $(DIROBJ)$(DIRPARSER)
	@mkdir -p $(DIROBJ)$(DIRELEM)
	@mkdir -p $(DIROBJ)$(DIRLIST)
	@git submodule init
	@git submodule update
	@make -s -C $(LIBVECT)
	@cd $(DIRLIB) && ln -f ../$(LIBVECT)/libvect.a .
	@ln -f $(LIBVECT)include/libvect.h $(INCLUDES)
	@make -s -C $(LIBFT)
	@cd $(DIRLIB) && ln -f ../$(LIBFT)/libft.a .
	@ln -f $(LIBFT)/includes/libft.h $(INCLUDES)
	@make -s -C $(LIBMLX)
	@cd $(DIRLIB) && ln -f ../$(LIBMLX)libmlx.a .
	@ln -f $(LIBMLX)mlx.h $(INCLUDES)mlx.h
	@make -s -C $(LIBOPT)
	@cd $(DIRLIB) && ln -f ../$(LIBOPT)libopt.a .
	@ln -f $(LIBOPT)/includes/getopt42.h $(INCLUDES)getopt42.h

end:
	@printf "\033[2K\033[1;36m%-20s\033[0;32m[Ready]\033[0m\n" $(NAME)

$(NAME): $(OBJ)
	@printf "\033[2KCompiling %s\r" $(NAME)
	@$(CC) $(FLAGS) -o $(DIRBIN)$@ $^ -I$(INCLUDES) -L$(DIRLIB) -lvect -lft -lmlx -lopt -lpthread $(MLXFLAGS)
	@ln -f $(DIRBIN)$(NAME) $(NAME)

$(DIROBJ)%.o: $(DIRSRC)%.c
	@printf "\r\033[2KLinking %s" $@
	@$(CC) $(FLAGS) -o $@ -c $< -I$(INCLUDES)

clean:
	@rm -f $(OBJ)

fclean: clean
	@rm -rf $(NAME)
	@rm -rf $(DIROBJ)
	@rm -rf $(DIRLIB)
	@rm -rf $(ARCHIVE_TEXTURE_NAME)

re: fclean all

lre:
	@rm -rf $(DIRLIB)
	@make -C $(LIBFT) re
	@make -C $(LIBVECT) re
	@make re

viewer:
	gcc -o viewer_xpm tools/xpm_viewver.c $(MLXFLAGS) -Llib/ -lmlx

dl_textures:
	curl -O https://dl.dropboxusercontent.com/s/kc26hjdlfz59v51/$(ARCHIVE_TEXTURE_NAME)
	tar -xf $(ARCHIVE_TEXTURE_NAME)
