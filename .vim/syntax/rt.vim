"Syntax file
" Language: Celestia Star Catalogs
" Maintainer: Kevin Lauder
" Latest Revision: 26 April 2008

if exists("b:current_syntax")
  finish
endif


" Keywords
syn keyword rtELEM -camera -sphere -cylinder -spot -cone -plane
syn keyword rtATTR ambiant_color diffuse_color specular_color size radius center direction texture name file reflection siz_tex file

" Matches
" syn match syntaxElementMatch 'regexp' contains=syntaxElement1 nextgroup=syntaxElement2 skipwhite
syn match rtComment "#.*$"
syn match rtEND "!.*$"

" Regions
syn region syntaxElementRegion start='-' end='!' fold transparent


let b:current_syntax = "rt"

hi def link rtATTR       Constant
hi def link rtELEM       Statement
hi def link rtComment    Comment
hi def link rtEND        Type
