let s:asciiart = [
 			\" ▄▄▄▄▄▄▄▄▄▄▄ ",
			\"▐░░░░░░░░░░░▌",
			\"▐░█▀▀▀▀▀▀▀█░▌",
			\"▐░▌       ▐░▌",
			\"▐░█▄▄▄▄▄▄▄█░▌",
			\"▐░░░░░░░░░░░▌",
			\"▐░█▀▀▀▀█░█▀▀ ",
			\"▐░▌     ▐░▌  ",
			\"▐░▌      ▐░▌ ",
			\"▐░▌       ▐░▌",
			\" ▀ bernand ▀ "
			\]

let s:styles = [
			\{
			\'extensions': ['\.c$', '\.h$', '\.cc$', '\.hh$', '\.cpp$', '\.hpp$'],
			\'start': '/*', 'end': '*/', 'fill': '*'
			\},
			\{
			\'extensions': ['\.htm$', '\.html$', '\.xml$'],
			\'start': '<!--', 'end': '-->', 'fill': '*'
			\},
			\{
			\'extensions': ['\.js$'],
			\'start': '//', 'end': '//', 'fill': '*'
			\},
			\{
			\'extensions': ['\.tex$'],
			\'start': '%', 'end': '%', 'fill': '*'
			\},
			\{
			\'extensions': ['\.ml$', '\.mli$', '\.mll$', '\.mly$'],
			\'start': '(*', 'end': '*)', 'fill': '*'
			\},
			\{
			\'extensions': ['\.vim$', 'vimrc$', '\.myvimrc$', 'vimrc$'],
			\'start': '"', 'end': '"', 'fill': '*'
			\},
			\{
			\'extensions': ['\.el$', '\.emacs$', '\.myemacs$'],
			\'start': ';', 'end': ';', 'fill': '*'
			\}
			\]

let s:linelen		= 80
let s:marginlen		= 2
let s:asciilen 		= 13
let s:contentlen	= s:linelen - (3 * s:marginlen - 1) - s:asciilen

function s:trimlogin ()
	let l:trimlogin = strpart($USER, 0, 9)
	if strlen(l:trimlogin) == 0
		let l:trimlogin = "marvin"
	endif
	return l:trimlogin
endfunction

function s:trimemail ()
	let l:trimemail = strpart($MAIL, 0, s:contentlen - 16)
	if strlen(l:trimemail) == 0
		let l:trimemail = "marvin@42.fr"
	endif
	return l:trimemail
endfunction

function s:midgap ()
	return repeat(' ', s:marginlen - 1)
endfunction

function s:lmargin ()
	return repeat(' ', s:marginlen - strlen(s:start))
endfunction

function s:rmargin ()
	return repeat(' ', s:marginlen - strlen(s:end))
endfunction

function s:empty_content ()
	return repeat(' ', s:contentlen)
endfunction

function s:left ()
	return s:start . s:lmargin()
endfunction

function s:right ()
	return s:rmargin() . s:end
endfunction

function s:bigline (line)
	return s:start . ' ' . repeat(s:fill, s:linelen - 3 - s:asciilen - strlen(s:start) - strlen(s:end)) . ' ' . s:asciiart[a:line] .' ' . s:end
endfunction

function s:logo (line)
	return s:left() . s:empty_content() . s:midgap() . s:asciiart[a:line] . s:right()
endfunction

function s:fileline (line)
	let l:trimfile = strpart(fnamemodify(bufname('%'), ':t'), 0, s:contentlen)
	return s:left() . l:trimfile . repeat(' ', s:contentlen - strlen(l:trimfile)) . s:midgap() . s:asciiart[line] . s:right()
endfunction


function s:coderline (line)
	let l:contentline = "By: ". s:trimlogin () . ' <' . s:trimemail () . '>'
	return s:left() . l:contentline . repeat(' ', s:contentlen - strlen(l:contentline)) . s:midgap() . s:asciiart[a:line] . s:right()
endfunction

function s:dateline (prefix, logo)
	let l:date = strftime("%Y/%m/%d %H:%M:%S")
	let l:contentline = a:prefix . ": " . l:date . " by " . s:trimlogin ()
	return s:left() . l:contentline . repeat(' ', s:contentlen - strlen(l:contentline)) . s:midgap() . s:asciiart[a:logo] . s:right()
endfunction

function s:createline (line)
	return s:dateline("Created", a:line)
endfunction

function s:updateline (line)
	return s:dateline("Updated", a:line)
endfunction

function s:emptyline (line)
	return s:start . repeat(' ', s:linelen - 1 - strlen(s:start) - s:asciilen - strlen(s:end)) . s:asciiart[a:line] . ' ' . s:end
endfunction

function s:filetype ()
	let l:file = fnamemodify(bufname("%"), ':t')

	let s:start = '#'
	let s:end = '#'
	let s:fill = '*'

	for l:style in s:styles
		for l:ext in l:style['extensions']
			if l:file =~ l:ext
				let s:start = l:style['start']
				let s:end = l:style['end']
				let s:fill = l:style['fill']
			endif
		endfor
	endfor
endfunction

function s:insert ()
	call s:filetype ()

	call append(0, "")
	call append (0, s:bigline(10))
	call append (0, s:emptyline(9))
	call append (0, s:updateline(8))
	call append (0, s:createline(7))
	call append (0, s:logo(6))
	call append (0, s:coderline(5))
	call append (0, s:logo(4))
	call append (0, s:fileline(3))
	call append (0, s:logo(2))
	call append (0, s:emptyline(1))
	call append (0, s:bigline(0))
endfunction

function s:update ()
	call s:filetype ()

	let l:pattern = s:start . repeat(' ', 2 - strlen(s:start)) . "Updated: [0-9]"
	let l:line = getline (9)
	if l:line =~ l:pattern
		call setline(9, s:updateline(8))
	endif
endfunction


command PvHeader call s:insert ()
nmap <F2> :PvHeader<CR>
autocmd BufNewFile *.rt call s:insert ()
autocmd BufWritePre *.rt call s:update ()
