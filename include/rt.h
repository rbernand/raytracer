/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 12:22:16 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 07:15:30 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_H
# define RT_H

# include <stdio.h>
# include <semaphore.h>

# include <pthread.h>
# include <libft.h>
# include <libvect.h>
# include <elements.h>
# include <mymlx.h>

typedef struct s_env		t_env;
typedef struct s_scene		t_scene;
typedef struct s_group		t_group;

# define MIN(X,Y)			((X) < (Y) ? (X) : (Y))
# define MAX(X,Y)			((X) > (Y) ? (X) : (Y))
# define FILTER(X, Y, Z)	(MIN(MAX((Y), (X)), (Z)))

struct						s_group
{
	char			*name;
	t_element		*elems;
};

struct						s_scene
{
	t_scene				*next;
	size_t				id;
	char				*name;
	ssize_t				sampling;
	ssize_t				depth;
	float				gamma;
	t_group				*groups;
	t_element			*elems;
	t_cam				cam;
	t_window			win;
	t_img				*imgs;
	t_texture			*textures;
	size_t				counter;
	size_t				win_x;
	size_t				win_y;
	size_t				threads;
	void				(*dump)(t_scene *);
	void				(*put_bar)(t_scene *);
	void				(*free_scn)(t_scene *);
};

struct						s_env
{
	t_mlx				mlx;
	t_scene				*scenes;
	pthread_t			*threads;
	char				verbose;
	size_t				interactive;
	sem_t				*thread_id;
};

const char					*random_semaphore(void);

t_env						*get_env(t_env *env);
t_mlx						*get_mlx(t_mlx *mlx);
t_scene						*current_scene(t_scene *scn);
void						set_signal_handler(void);
void						exit_rt(void);
t_return					mlx_create_image(t_mlx *mlx, t_scene *scene);
t_return					mlx_display_result(t_mlx *mlx, t_scene *scn);
t_return					mlx_create_window(t_scene *scn);

t_cam						reposition(t_cam target, t_element *ref);
t_vect3f					get_intersection_point(t_cam cam, float dist);

void						render(t_env *env, t_scene *scn);
t_color						trace(t_cam cam, t_element *element, t_color color);
t_color						phong_shading(t_element *elem, t_element *spot,
								t_vect3f inter, t_color base_color);
t_color						get_color(t_element *elem, t_element *elems,
								t_vect3f inter);
t_color						get_shadow(t_element *elements, t_element *elem,
								t_element *spot, t_vect3f inter);
double						perlin_noise(double x, double y, double z);

#endif
