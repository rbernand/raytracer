/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/08/23 18:55:15 by rbernand          #+#    #+#             */
/*   Updated: 2015/10/14 19:58:21 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROMPT_H
# define PROMPT_H

# include <libft.h>
# include <rt.h>

# define CYAN				"\033[36m"
# define RED				"\033[31m"
# define BLUE				"\033[34m"
# define NORMAL				"\033[0m"

typedef enum e_command		t_command;
enum						e_command
{
	_CMD_ERROR = -1,
	_EXIT = 0,
	_HELP,
	_CD,
	_SAVE,
	_MAKE,
	_LIST,
	_LS,
	_CLOSE,
	_FILTER,
	_NB_COMMANDS
};

typedef int					(*t_exec_cmd)(char **);

t_return					prompt(void);

t_return					save(char **args);
t_return					make(char **args);
t_return					exit_wrapper(char **cmd);
t_return					list_scenes(char **cmd);
t_return					ls(char **cmd);
t_return					close_cmd(char **cmd);
t_return					filter(char **cmd);

t_scene						*get_scenes_by_id(size_t id);

#endif
