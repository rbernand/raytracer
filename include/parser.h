/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/04 13:27:47 by etermeau          #+#    #+#             */
/*   Updated: 2016/03/09 00:46:03 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

# include <libft.h>
# include <elements.h>
# include <rt.h>

typedef struct s_parser			t_parser;
typedef enum e_type_parser		t_type_parser;
typedef enum e_type_param		t_type_param;

typedef int						(*t_factory)();

enum							e_type_parser
{
	_WAITING = -1,
	_CONFIG = _MAX_TYPE_ELEM,
	_POINT,
	_CAMERA,
	_TEXTURE,
	_GROUP,
	_MAX_TYPE_PARSER
};

enum							e_type_param
{
	_INVALID = -1,
	_CENTER,
	_SIZE,
	_RADIUS,
	_AMBIANTE_COLOR,
	_DIFFUSE_COLOR,
	_SPECULAR_COLOR,
	_DIRECTION,
	_NAME,
	_FILE,
	_TEX,
	_REFLEXION,
	_REFRACTION,
	_TRANSPARENCY,
	_SAMPLING,
	_DEPTH,
	_THREADS,
	_TEXTURE_SIZE,
	_BUMP,
	_GAMMA,
	_MAX_TYPE_PARAM
};

struct							s_parser
{
	char						*name;
	char						*file;
	char						*texture;
	t_type_parser				type;
	t_vect3f					center;
	t_vect3f					dir;
	t_vect3f					normal;
	t_vect3f					size;
	float						radius;
	t_color						ambiante_color;
	t_color						specular_color;
	t_color						diffuse_color;
	size_t						reflection;
	size_t						transparency;
	float						refraction;
	size_t						sampling;
	size_t						depth;
	size_t						threads;
	t_vect3f					texture_size;
	char						*bump;
	float						gamma;
	t_parser					*next;
};

extern const char				*g_params[_MAX_TYPE_PARAM];

t_scene							*create_scene(char *input_file);
t_element						*load_scene(char *file, t_scene *scn);

t_parser						*parse_elem(int fd, t_type_parser type);
t_element						*convert_parser(t_scene *scn, t_parser *lst,
									t_cam *cam, t_texture **texs);
t_return						load_texture(t_mlx *mlx, t_texture *texture);
t_return						link_elements(t_element *element,
									t_parser *parser, t_texture *textures);
t_element						*new_elemts_from_parser(t_parser *lst,
									t_texture *textures, t_element **objs);

t_color							color_factory(char *str);
t_vect3f						vecteur_factory(char *str);
t_return						diffuse_color_factory(t_parser *el, char *str);
t_return						ambiante_color_factory(t_parser *el, char *str);
t_return						specular_color_factory(t_parser *el, char *str);
t_return						center_factory(t_parser *elem, char *str);
t_return						size_factory(t_parser *elem, char *str);
t_return						radius_factory(t_parser *elem, char *str);
t_return						direction_factory(t_parser *elem, char *str);
t_return						name_factory(t_parser *elem, char *str);
t_return						file_factory(t_parser *elem, char *str);
t_return						texture_factory(t_parser *elem, char *str);
t_return						texture_size_factory(t_parser *elem, char *str);
t_return						reflection_factory(t_parser *elem, char *str);
t_return						refraction_factory(t_parser *elem, char *str);
t_return						sampling_factory(t_parser *elem, char *str);
t_return						depth_factory(t_parser *elem, char *str);
t_return						threads_factory(t_parser *elem, char *str);
t_return						transparency_factory(t_parser *elem, char *str);
t_return						bump_factory(t_parser *elem, char *str);
t_return						gamma_factory(t_parser *elem, char *str);

#endif
