/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filter.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: etermeau <etermeau@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/25 17:22:06 by etermeau          #+#    #+#             */
/*   Updated: 2016/03/07 12:58:42 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILTER_H
# define FILTER_H

# include <sys/types.h>
# include <rt.h>

# define FILTER_MAX_HEIGHT 10
# define FILTER_MAX_WIDTH 10

typedef	struct s_filter	t_filter;

struct					s_filter
{
	size_t				width;
	size_t				height;
	float				factor;
	float				bias;
	float				value[FILTER_MAX_HEIGHT][FILTER_MAX_WIDTH];
};

enum					e_filter
{
	_INVALID = -1,
	_BLUR = 0,
	_MOTION_BLUR,
	_SHARPEN,
	_MAX_FILTER
};

extern const char		*g_filter_name[_MAX_FILTER];

void					apply_filter(t_scene *scn, enum e_filter id);

#endif
