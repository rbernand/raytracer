/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mymlx.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 14:14:39 by rbernand          #+#    #+#             */
/*   Updated: 2016/02/21 10:28:34 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MYMLX_H
# define MYMLX_H

# include <pthread.h>
# include "libft.h"

# define KEY_ESC			53

# define MYMLX_BLACK		g_black
# define MYMLX_WHITE		g_white
# define MYMLX_RED			g_red
# define MYMLX_GREEN		g_green
# define MYMLX_BLUE			g_blue

typedef union u_color		t_color;
typedef struct s_point		t_point;
typedef struct s_img		t_img;
typedef struct s_mlx		t_mlx;
typedef struct s_window		t_window;
typedef struct s_scene		t_scene;

struct						s_color
{
	unsigned char			b;
	unsigned char			g;
	unsigned char			r;
	unsigned char			a;
};

union						u_color
{
	struct s_color			rgba;
	unsigned char			array[4];
	unsigned int			hexa;
};

struct						s_point
{
	unsigned int			x;
	unsigned int			y;
	t_color					color;
};

struct						s_img
{
	t_img					*next;
	char					*ptr;
	char					*data;
	int						endian;
	int						size_line;
	int						size_xy[2];
	int						bp;
};

struct						s_window
{
	t_window				*next;
	char					*title;
	void					*ptr;
	size_t					id;
	t_scene					*parent_scn;
};

struct						s_mlx
{
	void					*ptr;
	t_window				*windows;
};

t_color						mlx_get_value(const t_img *img, int x, int y);
void						mlx_put_point(const t_img *img, const t_point *p);

char						*str_color(t_color c);
t_color						sum_color(t_color a, t_color b);
t_color						prod_color(t_color a, float ratio);
t_color						mix_color(t_color dst, t_color src,
							float a, float b);
t_color						max_color(t_color a, t_color b);

extern const t_color		g_black;
extern const t_color		g_white;
extern const t_color		g_red;
extern const t_color		g_green;
extern const t_color		g_blue;

#endif
