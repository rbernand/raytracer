/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elements.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <vcosson@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 12:34:45 by rbernand          #+#    #+#             */
/*   Updated: 2016/03/09 02:36:17 by rbernand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ELEMENTS_H
# define ELEMENTS_H

# include <libft.h>
# include <libvect.h>
# include <mymlx.h>

typedef struct s_element		t_element;
typedef struct s_cam			t_cam;
typedef struct s_texture		t_texture;

typedef t_element				*(*t_elem_constructor)(t_vect3f, t_vect3f,
									t_vect3f, t_color);

struct							s_texture
{
	char					*name;
	char					*input_file;
	t_img					img;
	t_texture				*next;
};

struct							s_cam
{
	t_vect3f				ori;
	t_vect3f				dir;
	t_element				*hitted;
	int						depth;
};

enum							e_type
{
	_SPHERE = 0,
	_SPOT,
	_PLANE,
	_CYLINDER,
	_CONE,
	_DISK,
	_BLOB,
	_CUBE,
	_MAX_TYPE_ELEM
};

struct							s_element
{
	t_element			*next;
	char				*name;
	char				*group;
	enum e_type			type;
	t_vect3f			center;
	t_vect3f			dir;
	t_vect3f			size;
	t_color				ambiante_color;
	t_color				diffuse_color;
	t_color				specular_color;
	t_color				emission_color;
	t_texture			*texture;
	t_texture			*bump;
	t_element			*blobs;
	t_vect3f			texture_size;
	char				perlin : 1;
	size_t				reflection;
	size_t				transparency;
	float				refraction;
	t_return			(*intersect)(t_element *, t_cam, float *);
	t_vect3f			(*normale)(t_element *, t_vect3f);
	t_color				(*get_color)(t_element *, t_vect3f);
	t_vect3f			(*uv_mapping)(t_element *, t_vect3f, t_vect3f, char);
	void				(*dump)(t_element *);
	t_vect3f			(*bump_mapping)(t_element *, t_vect3f, t_vect3f);
	void				(*free_elem)(t_element *);
};

t_element						*new_element(enum e_type type);
t_element						*new_sphere(t_vect3f center, t_vect3f dir,
									t_vect3f size, t_color color);
t_element						*new_spot(t_vect3f center, t_vect3f dir,
									t_vect3f size, t_color color);
t_element						*new_cylinder(t_vect3f center, t_vect3f dir,
									t_vect3f size, t_color color);
t_element						*new_cone(t_vect3f center, t_vect3f dir,
									t_vect3f size, t_color color);
t_element						*new_plane(t_vect3f center, t_vect3f dir,
									t_vect3f size, t_color color);
t_element						*new_disk(t_vect3f center, t_vect3f dir,
									t_vect3f size, t_color color);
t_element						*new_blob(t_vect3f center, t_vect3f dir,
									t_vect3f size, t_color color);

#endif
