/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   xpm_viewver.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vcosson <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 13:35:54 by vcosson           #+#    #+#             */
/*   Updated: 2016/03/16 13:41:04 by vcosson          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <stdlib.h>
#include <stdio.h>

static int			key_hook(int key, void *mlx)
{
	(void)mlx;
	if (key == 53)
		exit(0);
	return (1);
}

int					main(int ac, char **av)
{
	void			*mlx;
	void			*win;
	void			*img;
	int				width;
	int				height;

	mlx = mlx_init();
	if ((img = mlx_xpm_file_to_image(mlx, av[1], &width, &height)))
	{
		if ((win = mlx_new_window(mlx, width, height, av[1])))
		{
			mlx_put_image_to_window(mlx, win, img, 0, 0);
			mlx_key_hook(win, &key_hook, mlx);
			mlx_loop(mlx);
		}
	}
}
