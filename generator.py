#!/busr/bin/env python3
from math import sin, cos, pi
from random import randint
class color:
    def __init__(self, r=0, g=0, b=0, a=0):
        self.r = r
        self.g = g
        self.b = b
        self.a = a

    def __str__(self):
        return ("{} {} {} {}".format(self.r, self.g, self.b, self.a))

class vecteur:
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return ("{0:.2f} {1:.2f} {2:.2f}".format(self.x, self.y, self.z))




class Element:
    def __init__(self, type_elem, center=vecteur(), size=vecteur(1), direction=vecteur(), ambiant=color(), diffuse=color(), specular=color(), refraction=1.0, reflection=0):
        self.type = type_elem
        self.center = center
        self.size = size
        self.direction = direction
        self.ambiant = ambiant
        self.diffuse = diffuse
        self.specular = specular
        self.reflection =reflection
        self.refraction = refraction

    def dump(self, target):
        target.write("+{}\n"\
                "\tcenter: vecteur {}\n" \
                "\tsize: vecteur {}\n" \
                "\tdirection: vecteur {}\n" \
                "\tambiant_color: rgba {}\n" \
                "\tdiffuse_color: rgba {}\n" \
                "\tspecular_color: rgba {}\n" \
                "\treflection: ratio {}\n" \
                "\trefraction: ratio {}\n" \
                "!\n".format(self.type,
                    self.center, self.size, self.direction, self.ambiant, self.diffuse, self.specular, self.reflection, self.refraction)
            )

class Camera:
    def __init__(self, pos=vecteur(), direction=vecteur()):
        self.pos = pos
        self.direction = direction

    def dump(self, target):
        target.write("+camera\n"\
                "\tcenter: vecteur {}\n" \
                "\tdirection: vecteur {}\n" \
                "!\n".format(self.pos, self.direction)
            )

class Config:
    def __init__(self, name="untitled", depth=5, sampling=1, winx=1280, winy=1024):
        self.name = name
        self.depth = depth
        self.sampling = sampling
        self.size = vecteur(winx, winy, 0)

    def dump(self, target):
        target.write("+config\n"\
                "\tname: text \"{}\"\n"\
                "\tsize: vecteur {}\n"\
                "\tsampling: value {}\n"\
                "\tdepth: value {}\n"\
                "!\n".format(self.name, self.size, self.sampling, self.depth))

class Scene:
    def __init__(self, config, camera):
        self.elem = []
        self.config = config
        self.camera = camera

    def add(self, elem):
        self.elem.append(elem)

    def write(self, output):
        with open(output, 'w+') as stream:
            self.config.dump(stream)
            self.camera.dump(stream)
            for elem in self.elem:
                elem.dump(stream)

def randomColor():
    return (color(randint(0, 255), randint(0, 255), randint(0, 255), 100))

WHITE = color(255, 255, 255, 100)

def multisphere():
    conf = Config("Test Python Generator", 5, 1, 2000, 1600)
    cam = Camera(vecteur(0, 20, 100), vecteur(45, 0, 0))
    scn = Scene(conf, cam)
    nb_items = 1
    spot = Element("spot", ambiant=WHITE, center=vecteur(0, 0, -5))
    plane= Element("plane", ambiant=color(200, 125, 0, 30), specular=WHITE,size=vecteur(), center=vecteur(0, 0 , 15), direction=vecteur(0, 0, 90), reflection=50)
    for h in range(-5, 10):
        for i in range(nb_items):
            x = (10 + h) * sin(i * 2 * pi /nb_items)
            y = (10) * cos(i * 2 * pi /nb_items)
            tmp_color = randomColor()
            scn.add(Element("sphere", size=vecteur(0.3, 0, 0), center=vecteur(x, y, h), diffuse=tmp_color, ambiant=tmp_color, specular=WHITE, reflection=50))

    scn.add(plane)
    scn.add(spot)

    scn.write("test.rt")

class Cube:
    def __init__(self, size=vecteur(1, 1), center=vecteur()):
        self.c = []
        self.c.append(Element("plane", direction=vecteur(90, 0, 0), size=vecteur(size.x, size.x, 0), center=vecteur(0, size.x, 10), ambiant=color(255, 255, 255, 100)))
        self.c.append(Element("plane", direction=vecteur(-90, 0, 0), size=vecteur(size.x, size.x, 0), center=vecteur(0, -size.x, 10), ambiant=color(255, 0, 255, 100)))
        self.c.append(Element("plane", direction=vecteur(0, 90, 0), size=vecteur(size.x, size.x, 0), center=vecteur(-size.x, 0, 10), ambiant=color(0, 0, 255, 100)))
        self.c.append(Element("plane", direction=vecteur(0, -90, 0), size=vecteur(size.x, size.x, 0), center=vecteur(size.x, 0, 10), ambiant=color(255, 255, 0, 100)))
        self.c.append(Element("plane", direction=vecteur(0, 0, 90), size=vecteur(size.x, size.x, 0), center=vecteur(0, 0, 10 + size.x), ambiant=color(255, 0, 0, 100)))
        self.c.append(Element("plane", direction=vecteur(0, 0, -90), size=vecteur(size.x, size.x, 0), center=vecteur(0, 0, 10 - size.x), ambiant=color(0, 255, 0, 100)))

    def dump(self, target):
        for face in self.c:
            face.dump(target)

def multicube():
    conf = Config("Test Python Generator", 5, 1, 2000, 1600)
    cam = Camera(vecteur(5, 20, -10), vecteur(45, 0, 0))
    scn = Scene(conf, cam)
    c = Cube(size=vecteur(3, 3))
    scn.add(c)
    scn.write("test2.rt")

def main():
   multisphere()
    # multicube()

if __name__ == "__main__":
    main()
